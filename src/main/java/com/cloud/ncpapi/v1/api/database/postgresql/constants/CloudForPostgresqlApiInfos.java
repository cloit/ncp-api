package com.cloud.ncpapi.v1.api.database.postgresql.constants;

import lombok.Getter;

public enum CloudForPostgresqlApiInfos {

    CLOUD_POSTGRESQL_INSTANCE_LIST(
            "CLOUD_POSTGRESQL_INSTANCE_LIST"
            , "PostgreSQL 인스턴스 목록"
            , "https://ncloud.apigw.ntruss.com"
            , "/vpostgresql/v2/getCloudPostgresqlInstanceList"
            , "GET"
    ),
    CLOUD_POSTGRESQL_INSTANCE_DETAILS(
            "CLOUD_POSTGRESQL_INSTANCE_DETAILS"
            , "PostgreSQL 인스턴스 상세정보"
            , "https://ncloud.apigw.ntruss.com"
            , "/vpostgresql/v2/getCloudPostgresqlInstanceDetail"
            , "GET"
    )
    ;
    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    CloudForPostgresqlApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
