package com.cloud.ncpapi.v1.api.networking.vpc.vpcmanagement.constants;

import lombok.Getter;

public enum VpcManagementApiInfos {

    VPC_LIST(
            "VPC_LIST"
            , "VPC 목록 조회"
            , "https://ncloud.apigw.ntruss.com"
            , "/vpc/v2/getVpcList"
            , "GET"
    )
    ;

    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    VpcManagementApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
