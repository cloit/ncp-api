package com.cloud.ncpapi.v1.api.server.common.domain.response.region;

import lombok.Data;

@Data
public class ServerRegion {

    private String regionCode;

    private String regionName;
}
