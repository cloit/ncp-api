package com.cloud.ncpapi.v1.api.networking.loadbalancer.domain.response;

import lombok.Data;

@Data
public class LoadBalancerSubnet {

    private String zoneCode;

    private String subnetNo;

    private String publicIpInstanceNo;
}
