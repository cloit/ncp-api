package com.cloud.ncpapi.v1.api.management.insight.domain.response;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class DataQueryResponseMetricInfo {

    private String aggregation;

    private Map<String, String> dimensions;

    private List<List<Double>> dps;

    private String interval;

    private String metric;

    private String productName;
}
