package com.cloud.ncpapi.comm.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;

public class GsonUtils {

    private static final Gson GSON = new GsonBuilder().create();;

    /**
     * Object -> json
     * @param obj json 으로 변활할 Object
     * @return json 리턴
     */
    public static String toJson(Object obj) {
        return GSON.toJson(obj);
    }

    /**
     * json -> Object
     * @param json json 문자열
     * @param type 변환될 type
     * @return type class 리턴
     */
    public static <T> T fromJson(String json, Type type) {
        return GSON.fromJson(json, type);
    }
}
