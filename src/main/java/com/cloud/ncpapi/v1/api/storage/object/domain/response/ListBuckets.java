package com.cloud.ncpapi.v1.api.storage.object.domain.response;

import lombok.Data;

@Data
public class ListBuckets {

    private Owner owner;

    private Buckets buckets;
}
