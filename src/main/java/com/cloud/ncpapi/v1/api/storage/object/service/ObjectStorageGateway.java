package com.cloud.ncpapi.v1.api.storage.object.service;

import com.cloud.ncpapi.comm.component.http.client.HttpClient;
import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.comm.component.http.domain.HttpResponse;
import com.cloud.ncpapi.comm.component.http.gateway.Gateway;
import com.cloud.ncpapi.v1.api.storage.object.domain.ObjectStorageAuthInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ObjectStorageGateway implements Gateway<ObjectStorageAuthInfo> {

    private final HttpClient httpClient;

    public ObjectStorageGateway(@Qualifier("poolingHttpClient") HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    @Override
    public void preExecute(HttpRequest httpRequest, ObjectStorageAuthInfo objectStorageAuthInfo) {
        String accessKey    = objectStorageAuthInfo.getAccessKey();
        String secretKey    = objectStorageAuthInfo.getSecretKey();
        String region       = objectStorageAuthInfo.getRegionCode();
        try{
            AwsRequestSigningService.authorization(httpRequest, region, accessKey, secretKey);
        }catch (Exception e){
            log.debug("##### Object Storage Aws Signature Setting Exception ", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public HttpResponse execute(HttpRequest httpRequest) {
        return httpClient.execute(httpRequest);
    }

    @Override
    public void postExecute(HttpResponse httpResponse) {

    }
}
