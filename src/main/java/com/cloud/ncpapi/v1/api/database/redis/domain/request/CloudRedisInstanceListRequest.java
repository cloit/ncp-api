package com.cloud.ncpapi.v1.api.database.redis.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class CloudRedisInstanceListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String zoneCode;

    private String vpcNo;

    private String subnetNo;

    private String cloudRedisServiceName;

    private List<String> cloudRedisInstanceNoList;

    private String cloudRedisServerName;

    private List<String> cloudRedisServerInstanceNoList;

    private String generationCode;

    private Integer pageNo;

    private Integer pageSize;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
