package com.cloud.ncpapi.v1.api.database.redis.service;

import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.v1.api.comm.data.Validation;
import com.cloud.ncpapi.v1.api.database.redis.constants.CloudForRedisApiInfos;
import com.cloud.ncpapi.v1.api.database.redis.domain.request.CloudRedisInstanceListRequest;
import com.cloud.ncpapi.v1.api.database.redis.domain.response.CloudRedisInstanceListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class CloudForRedisService {

    private final NcpHttpService ncpHttpService;

    @Value("${ncp.properties.api-host}")
    private String apiHost;

    /**
     * Redis 인스턴스 조회
     * @param cloudRedisInstanceListRequest request 정보
     * @return CloudRedisInstanceListResponse
     */
    public CloudRedisInstanceListResponse getCloudRedisInstanceList(CloudRedisInstanceListRequest cloudRedisInstanceListRequest){

        Validation.apiKeyCheck(cloudRedisInstanceListRequest);

        final String path = CloudForRedisApiInfos.CLOUD_REDIS_INSTANCE_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(cloudRedisInstanceListRequest);

        return ncpHttpService.httpExecute(httpRequest, cloudRedisInstanceListRequest, CloudRedisInstanceListResponse.class);
    }
}
