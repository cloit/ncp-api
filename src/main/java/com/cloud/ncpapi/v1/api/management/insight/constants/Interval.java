package com.cloud.ncpapi.v1.api.management.insight.constants;

import lombok.Getter;

import java.util.Arrays;

public enum Interval {

    Min1("Min1"),
    Min5("Min5"),
    Min30("Min30"),
    Hour2("Hour2"),
    Day1("Day1");

    @Getter
    private final String codeId;

    Interval(String codeId) {
        this.codeId = codeId;
    }

    public static Interval find(String codeId){
        return Arrays.stream(values())
                .filter(interval -> interval.codeId.equals(codeId))
                .findAny()
                .orElseThrow(() -> new RuntimeException("Interval not found"));
    }
}
