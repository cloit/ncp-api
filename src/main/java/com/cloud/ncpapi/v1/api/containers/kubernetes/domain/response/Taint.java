package com.cloud.ncpapi.v1.api.containers.kubernetes.domain.response;

import lombok.Data;

@Data
public class Taint {

    private String key;

    private String effect;

    private String value;
}
