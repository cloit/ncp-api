package com.cloud.ncpapi.v1.api.containers.kubernetes.domain.response;

import lombok.Data;

@Data
public class AutoScale {

    private Boolean enabled;

    private Integer min;

    private Integer max;
}
