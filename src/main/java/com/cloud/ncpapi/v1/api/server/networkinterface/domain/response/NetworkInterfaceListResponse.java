package com.cloud.ncpapi.v1.api.server.networkinterface.domain.response;

import lombok.Data;

@Data
public class NetworkInterfaceListResponse {

    private NetworkInterfaceList getNetworkInterfaceListResponse;
}
