package com.cloud.ncpapi.v1.api.comm.data;

import java.util.LinkedHashMap;
import java.util.Map;

public class ResponseData {

    private final String RESULT_CODE = "resultCode";

    private final String RESULT_DATA = "data";

    private final Integer SUCCESS_CODE = 200;

    private final Integer FAIL_CODE = 400;

    private final Map<String, Object> responseMap;

    public ResponseData(){
        this.responseMap = new LinkedHashMap<>();
    }

    public void responseSuccess(Object value){
        responseMap.put(RESULT_CODE, SUCCESS_CODE);
        responseMap.put(RESULT_DATA, value);
    }

    public void responseFail(Exception e){
        responseMap.put(RESULT_CODE, FAIL_CODE);
        responseMap.put(RESULT_DATA, e.getMessage());
    }

    public Map<String, Object> responseResult(){
        return responseMap;
    }
}
