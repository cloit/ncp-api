package com.cloud.ncpapi.v1.api.database.postgresql.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class CloudPostgresqlInstanceListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String zoneCode;

    private String vpcNo;

    private String subnetNo;

    private String cloudPostgresqlServiceName;

    private List<String> cloudPostgresqlInstanceNoList;

    private String cloudPostgresqlServerName;

    private List<String> cloudPostgresqlServerInstanceNoList;

    private Integer pageNo;

    private Integer pageSize;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
