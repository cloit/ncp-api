package com.cloud.ncpapi.v1.api.server.common.domain.response.server.comm;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

@Data
public class Product {

    private String productCode;

    private String productName;

    private CommonCode productType;

    private String productDescription;

    private CommonCode infraResourceType;

    private CommonCode infraResourceDetailType;

    private Integer cpuCount;

    private Long memorySize;

    private Long baseBlockStorageSize;

    private CommonCode platformType;

    private String osInformation;

    private CommonCode diskType;

    private String dbKindCode;

    private Long addBlockStorageSize;

    private String generationCode;
}
