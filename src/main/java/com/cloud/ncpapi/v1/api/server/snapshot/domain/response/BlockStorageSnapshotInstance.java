package com.cloud.ncpapi.v1.api.server.snapshot.domain.response;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.Date;

@Data
public class BlockStorageSnapshotInstance {

    private String blockStorageSnapshotInstanceNo;

    private String blockStorageSnapshotName;

    private Long blockStorageSnapshotVolumeSize;

    private String originalBlockStorageInstanceNo;

    private CommonCode blockStorageSnapshotInstanceStatus;

    private CommonCode blockStorageSnapshotInstanceOperation;

    private String blockStorageSnapshotInstanceStatusName;

    private Date createDate;

    private Boolean isEncryptedOriginalBlockStorageVolume;

    private String blockStorageSnapshotDescription;

    private CommonCode snapshotType;

    private String baseSnapshotInstanceNo;

    private Integer snapshotChainDepth;

    private CommonCode hypervisorType;

    private Boolean isBootable;
}
