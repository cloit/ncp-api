package com.cloud.ncpapi.v1.api.networking.nacl.domain.response;

import lombok.Data;

@Data
public class VpcNaclListResponse {

    private NetworkAclList getNetworkAclListResponse;
}
