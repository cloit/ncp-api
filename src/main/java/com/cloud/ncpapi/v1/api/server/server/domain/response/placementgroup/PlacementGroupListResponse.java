package com.cloud.ncpapi.v1.api.server.server.domain.response.placementgroup;

import lombok.Data;

@Data
public class PlacementGroupListResponse {

    private PlacementGroupList getPlacementGroupListResponse;
}
