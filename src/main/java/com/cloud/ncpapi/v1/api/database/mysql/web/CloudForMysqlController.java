package com.cloud.ncpapi.v1.api.database.mysql.web;

import com.cloud.ncpapi.v1.api.comm.data.ResponseData;
import com.cloud.ncpapi.v1.api.database.mysql.constants.CloudForMysqlApiInfos;
import com.cloud.ncpapi.v1.api.database.mysql.domain.request.CloudMysqlInstanceDetailRequest;
import com.cloud.ncpapi.v1.api.database.mysql.domain.request.CloudMysqlInstanceListRequest;
import com.cloud.ncpapi.v1.api.database.mysql.domain.response.CloudMysqlInstanceDetailResponse;
import com.cloud.ncpapi.v1.api.database.mysql.domain.response.CloudMysqlInstanceListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpApiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/ncp/cdb/mysql")
@Slf4j
public class CloudForMysqlController {

    private final NcpApiService ncpApiService;

    /**
     * Mysql 인스턴스 조회
     * @param cloudMysqlInstanceListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/instances")
    public Mono<Map<String, Object>> instances(@RequestBody CloudMysqlInstanceListRequest cloudMysqlInstanceListRequest){
        log.debug("##### cloudMysqlInstanceListRequest : {}", cloudMysqlInstanceListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final CloudMysqlInstanceListResponse cloudMysqlInstanceListResponse
                    = ncpApiService.apiGetExecute(cloudMysqlInstanceListRequest, CloudMysqlInstanceListResponse.class, CloudForMysqlApiInfos.CLOUD_MYSQL_INSTANCE_LIST.getPath());
            responseData.responseSuccess(cloudMysqlInstanceListResponse);
        }catch (Exception e){
            log.debug("##### Mysql Instances Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    @RequestMapping("/details")
    public Mono<Map<String, Object>> details(@RequestBody CloudMysqlInstanceDetailRequest cloudMysqlInstanceDetailRequest){
        log.debug("##### cloudMysqlInstanceDetailRequest : {}", cloudMysqlInstanceDetailRequest);
        final ResponseData responseData = new ResponseData();
        try{
            final CloudMysqlInstanceDetailResponse cloudMysqlInstanceDetailResponse
                    = ncpApiService.apiGetExecute(cloudMysqlInstanceDetailRequest, CloudMysqlInstanceDetailResponse.class, CloudForMysqlApiInfos.CLOUD_MYSQL_INSTANCE_DETAILS.getPath());
            responseData.responseSuccess(cloudMysqlInstanceDetailResponse);
        }catch (Exception e){
            log.debug("##### Mysql Instance Details Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
