package com.cloud.ncpapi.v1.api.server.common.domain.request.server.product;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class ServerProductListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String zoneCode;

    private String serverImageProductCode;

    private String exclusionProductCode;

    private String productCode;

    private String generationCode;

    private String memberServerImageInstanceNo;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
