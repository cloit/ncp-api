package com.cloud.ncpapi.comm.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class ObjectMapperUtils {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Map -> Object
     * @param map map
     * @param type 변환될 객체 타입
     * @return T
     */
    public static <T> T mapToObject(Map<String, Object> map, Type type){
        TypeFactory typeFactory = objectMapper.getTypeFactory();
        return objectMapper.convertValue(map, typeFactory.constructType(type));
    }


    public static Map<String, Object> objectToMap(Object obj){
        return objectMapper.convertValue(obj, new TypeReference<>(){});
    }
}
