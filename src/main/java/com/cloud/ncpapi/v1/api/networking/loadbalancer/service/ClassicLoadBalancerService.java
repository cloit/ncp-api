package com.cloud.ncpapi.v1.api.networking.loadbalancer.service;

import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.v1.api.comm.data.Validation;
import com.cloud.ncpapi.v1.api.networking.loadbalancer.constants.ClassicLoadBalancerApiInfos;
import com.cloud.ncpapi.v1.api.networking.loadbalancer.domain.request.LoadBalancerInstanceListRequest;
import com.cloud.ncpapi.v1.api.networking.loadbalancer.domain.response.LoadBalancerInstanceListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ClassicLoadBalancerService {

    private final NcpHttpService ncpHttpService;

    @Value("${ncp.properties.api-host}")
    private String apiHost;

    /**
     * 로드밸런서(classic) 인스턴스 조회
     * @param loadBalancerInstanceListRequest request 정보
     * @return LoadBalancerInstanceListResponse
     */
    public LoadBalancerInstanceListResponse getLoadBalancerInstanceList(LoadBalancerInstanceListRequest loadBalancerInstanceListRequest){

        Validation.apiKeyCheck(loadBalancerInstanceListRequest);

        final String path = ClassicLoadBalancerApiInfos.CLASSIC_LOAD_BALANCER_INSTANCE_LIST.getPath();

        HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(loadBalancerInstanceListRequest);

        return ncpHttpService.httpExecute(httpRequest, loadBalancerInstanceListRequest, LoadBalancerInstanceListResponse.class);
    }
}
