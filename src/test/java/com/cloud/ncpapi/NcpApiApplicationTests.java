package com.cloud.ncpapi;

import com.cloud.ncpapi.comm.utils.GsonUtils;
import com.cloud.ncpapi.config.NcpApiApplication;
import com.cloud.ncpapi.v1.api.management.cat.constants.CloudActivityTracerApiInfos;
import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import com.cloud.ncpapi.v1.comm.service.NcpApiService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;

@SpringBootTest(classes = NcpApiApplication.class)
@Slf4j
class NcpApiApplicationTests {

    @Autowired
    private NcpApiService ncpApiService;

    @Test
    void catTest() {

        final NcpApiKeyInfo ncpApiKeyInfo = new NcpApiKeyInfo();
        ncpApiKeyInfo.setAccessKey("EE40BFFC9B8CE2ED3DCD");
        ncpApiKeyInfo.setSecretKey("6A8601DF0CEF94DF3FF604FF7E77990592DFD762");
        final String path = CloudActivityTracerApiInfos.ACTIVITY_LIST.getPath();

        Map<String, Object> responseMap = ncpApiService.ActivityTracerPostExecute(ncpApiKeyInfo, Map.class, path);
        log.debug("##### response data : {}", GsonUtils.toJson(responseMap));
    }

}
