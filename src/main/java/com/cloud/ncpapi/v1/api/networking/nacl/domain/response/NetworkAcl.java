package com.cloud.ncpapi.v1.api.networking.nacl.domain.response;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.Date;

@Data
public class NetworkAcl {

    private String networkAclNo;

    private String networkAclName;

    private String vpcNo;

    private CommonCode networkAclStatus;

    private String networkAclDescription;

    private Date createDate;

    private Boolean isDefault;
}
