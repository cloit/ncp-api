package com.cloud.ncpapi.v1.api.networking.loadbalancer.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class LoadBalancerListenerList {

    private Integer totalRows;

    private List<LoadBalancerListener> loadBalancerListenerList;
}
