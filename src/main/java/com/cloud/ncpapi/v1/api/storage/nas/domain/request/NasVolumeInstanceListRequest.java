package com.cloud.ncpapi.v1.api.storage.nas.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class NasVolumeInstanceListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String zoneCode;

    private List<String> nasVolumeInstanceNoList;

    private String volumeName;

    private String volumeAllotmentProtocolTypeCode;

    private Boolean isEventConfiguration;

    private Boolean isSnapshotConfiguration;

    private Integer pageNo;

    private Integer pageSize;

    private String sortedBy;

    private String sortingOrder;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
