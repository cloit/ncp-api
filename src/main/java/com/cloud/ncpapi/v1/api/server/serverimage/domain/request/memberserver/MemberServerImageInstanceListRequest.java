package com.cloud.ncpapi.v1.api.server.serverimage.domain.request.memberserver;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class MemberServerImageInstanceListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private List<String> memberServerImageInstanceNoList;

    private String memberServerImageName;

    private String memberServerImageInstanceStatusCode;

    private List<String> platformTypeCodelist;

    private Integer pageNo;

    private Integer pageSize;

    private String sortedBy;

    private String sortingOrder;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
