package com.cloud.ncpapi.v1.api.database.redis.web;

import com.cloud.ncpapi.v1.api.comm.data.ResponseData;
import com.cloud.ncpapi.v1.api.database.redis.constants.CloudForRedisApiInfos;
import com.cloud.ncpapi.v1.api.database.redis.domain.request.CloudRedisInstanceDetailRequest;
import com.cloud.ncpapi.v1.api.database.redis.domain.request.CloudRedisInstanceListRequest;
import com.cloud.ncpapi.v1.api.database.redis.domain.response.CloudRedisInstanceDetailResponse;
import com.cloud.ncpapi.v1.api.database.redis.domain.response.CloudRedisInstanceListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpApiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/ncp/cdb/redis")
@Slf4j
public class CloudForRedisController {

    private final NcpApiService ncpApiService;


    /**
     * Redis 인스턴스 조회
     * @param cloudRedisInstanceListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/instances")
    public Mono<Map<String, Object>> instances(@RequestBody CloudRedisInstanceListRequest cloudRedisInstanceListRequest){
        log.debug("##### cloudRedisInstanceListRequest : {}", cloudRedisInstanceListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final CloudRedisInstanceListResponse cloudRedisInstanceListResponse
                    = ncpApiService.apiGetExecute(cloudRedisInstanceListRequest, CloudRedisInstanceListResponse.class, CloudForRedisApiInfos.CLOUD_REDIS_INSTANCE_LIST.getPath());
            responseData.responseSuccess(cloudRedisInstanceListResponse);
        }catch (Exception e){
            log.debug("##### Redis Instances Exception", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    @RequestMapping("/details")
    public Mono<Map<String, Object>> details(@RequestBody CloudRedisInstanceDetailRequest cloudRedisInstanceDetailRequest){
        log.debug("##### cloudRedisInstanceDetailRequest : {}", cloudRedisInstanceDetailRequest);
        final ResponseData responseData = new ResponseData();
        try{
            final CloudRedisInstanceDetailResponse cloudRedisInstanceDetailResponse
                    = ncpApiService.apiGetExecute(cloudRedisInstanceDetailRequest, CloudRedisInstanceDetailResponse.class, CloudForRedisApiInfos.CLOUD_REDIS_INSTANCE_DETAILS.getPath());
            responseData.responseSuccess(cloudRedisInstanceDetailResponse);
        }catch (Exception e){
            log.debug("##### Redis Instance Details Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
