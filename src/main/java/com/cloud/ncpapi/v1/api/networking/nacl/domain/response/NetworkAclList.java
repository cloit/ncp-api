package com.cloud.ncpapi.v1.api.networking.nacl.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class NetworkAclList {

    private Integer totalRows;

    private List<NetworkAcl> networkAclList;
}
