package com.cloud.ncpapi.v1.api.storage.object.domain;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

@Data
public class ObjectStorageAuthInfo {

    /**
     * NCP API Access Key
     */
    private String accessKey;

    /**
     * NCP API Secret Key
     */
    private String secretKey;

    private String regionCode;

}
