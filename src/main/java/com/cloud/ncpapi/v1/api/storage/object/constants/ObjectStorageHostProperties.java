package com.cloud.ncpapi.v1.api.storage.object.constants;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@ConfigurationProperties(prefix = "ncp.properties")
public class ObjectStorageHostProperties {

    private Map<String, String> objectStorageHost;

    public Map<String, String> getObjectStorageHost() {
        return objectStorageHost;
    }

    public void setObjectStorageHost(Map<String, String> objectStorageHost) {
        this.objectStorageHost = objectStorageHost;
    }

    public String findObjectStorageHost(String regionCode){
        return objectStorageHost.get(regionCode);
    }
}
