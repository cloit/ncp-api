package com.cloud.ncpapi.v1.api.server.server.domain.response.loginkey;

import lombok.Data;

import java.util.Date;

@Data
public class LoginKey {

    private String fingerprint;

    private String keyName;

    private Date createDate;
}
