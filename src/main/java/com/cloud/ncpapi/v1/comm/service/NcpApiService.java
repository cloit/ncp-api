package com.cloud.ncpapi.v1.comm.service;

import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.v1.api.comm.data.Validation;
import com.cloud.ncpapi.v1.api.containers.kubernetes.domain.request.KubernetesRequest;
import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import io.micrometer.common.util.StringUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class NcpApiService {

    private final NcpHttpService ncpHttpService;

    @Value("${ncp.properties.api-host}")
    private String apiHost;

    @Value("${ncp.properties.k8s-api-host}")
    private String k8sApiHost;

    @Value("${ncp.properties.activity-tracer-host}")
    private String activityTracerHost;

    /**
     * 공통 API 호출
     * @param requestData request 데이터
     * @param responseType response type
     * @param path api path
     * @return R
     */
    public <T extends NcpApiKeyInfo, R> R apiGetExecute(T requestData, Type responseType, String path){

        Validation.apiKeyCheck(requestData);

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(requestData);

        return ncpHttpService.httpExecute(httpRequest, requestData, responseType);

    }

    /**
     * K8S API 호출
     * @param requestData request 데이터
     * @param responseType response type
     * @param path api path
     * @param pathParam path parameter
     * @return T
     */
    public <T extends KubernetesRequest, R> R k8sApiGetExecute(T requestData, Type responseType, String path, Map<String, String> pathParam){

        Validation.apiKeyCheck(requestData);

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(k8sApiHost)
                .setPath(path)
                .setQueryParams(requestData);

        String regionCode = requestData.getRegionCode();

        if(StringUtils.isNotEmpty(regionCode) && regionCode.equals("GCS01")){
            httpRequest.setHeader("x-ncp-region_code", regionCode);
        }

        if( pathParam != null ){
            for( String key : pathParam.keySet() ){
                httpRequest.setPathParam(key, pathParam.get(key));
            }
        }

        return ncpHttpService.httpExecute(httpRequest, requestData, responseType);
    }

    /**
     * Activity Tracer API 호출
     * @param requestData request 데이터
     * @param responseType response type
     * @param path api path
     * @return T
     */
    public <T extends NcpApiKeyInfo, R> R ActivityTracerPostExecute(T requestData, Type responseType, String path){

        Validation.apiKeyCheck(requestData);

        final HttpRequest httpRequest = HttpRequest.create()
                .post()
                .setHost(activityTracerHost)
                .setPath(path)
                .setEntity(requestData);

        return ncpHttpService.httpExecute(httpRequest, requestData, responseType);
    }

}
