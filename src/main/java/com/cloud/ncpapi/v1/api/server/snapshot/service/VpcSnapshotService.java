package com.cloud.ncpapi.v1.api.server.snapshot.service;

import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.v1.api.comm.data.Validation;
import com.cloud.ncpapi.v1.api.server.snapshot.constants.VpcSnapshotApiInfos;
import com.cloud.ncpapi.v1.api.server.snapshot.domain.request.BlockStorageSnapshotInstanceListRequest;
import com.cloud.ncpapi.v1.api.server.snapshot.domain.response.BlockStorageSnapshotInstanceListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class VpcSnapshotService {

    private final NcpHttpService ncpHttpService;

    @Value("${ncp.properties.api-host}")
    private String apiHost;

    /**
     * 블록 스토리지 스냅샷 인스턴스 조회
     * @param blockStorageSnapshotInstanceListRequest request 정보
     * @return BlockStorageSnapshotInstanceListResponse
     */
    public BlockStorageSnapshotInstanceListResponse getBlockStorageSnapshotInstanceList(BlockStorageSnapshotInstanceListRequest blockStorageSnapshotInstanceListRequest){

        Validation.apiKeyCheck(blockStorageSnapshotInstanceListRequest);

        final String path = VpcSnapshotApiInfos.VPC_BLOCK_STORAGE_SNAPSHOT_INSTANCE_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(blockStorageSnapshotInstanceListRequest);

        return ncpHttpService.httpExecute(httpRequest, blockStorageSnapshotInstanceListRequest, BlockStorageSnapshotInstanceListResponse.class);
    }
}
