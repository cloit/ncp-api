package com.cloud.ncpapi.v1.api.comm.domain;

import lombok.Data;

@Data
public class BlockStorageMapping {

    private Integer order;

    private String blockStorageSnapshotInstanceNo;

    private String blockStorageSnapshotInstanceName;

    private Long blockStorageSize;

    private String blockStorageName;

    private CommonCode blockStorageVolumeType;

    private Long iops;

    private Long throughput;

    private Boolean isEncryptedVolume;
}
