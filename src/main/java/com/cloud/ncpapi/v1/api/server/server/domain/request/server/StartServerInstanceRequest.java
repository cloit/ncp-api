package com.cloud.ncpapi.v1.api.server.server.domain.request.server;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class StartServerInstanceRequest extends NcpApiKeyInfo {

    private String regionCode;

    private List<String> serverInstanceNoList;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
