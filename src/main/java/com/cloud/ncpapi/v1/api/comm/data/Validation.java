package com.cloud.ncpapi.v1.api.comm.data;


import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import org.apache.commons.lang3.StringUtils;

public class Validation {

    /**
     * NCP API 키 정보 유효성 검사
     * @param ncpApiKeyInfo NCP api key 정보
     */
    public static void apiKeyCheck(NcpApiKeyInfo ncpApiKeyInfo){
        final String accessKey = ncpApiKeyInfo.getAccessKey();
        final String secretKey = ncpApiKeyInfo.getSecretKey();

        if(StringUtils.isEmpty(accessKey)){
            throw new RuntimeException("Access Key is empty");
        }

        if(StringUtils.isEmpty(secretKey)){
            throw new RuntimeException("Secret Key is empty");
        }
    }

}
