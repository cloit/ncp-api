package com.cloud.ncpapi.v1.api.server.storage.web;

import com.cloud.ncpapi.v1.api.comm.data.ResponseData;
import com.cloud.ncpapi.v1.api.server.storage.constants.VpcServerStorageApiInfos;
import com.cloud.ncpapi.v1.api.server.storage.domain.request.BlockStorageInstanceListRequest;
import com.cloud.ncpapi.v1.api.server.storage.domain.response.BlockStorageInstanceListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpApiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/vpc/blockStorage")
@RequiredArgsConstructor
@Slf4j
public class BlockStorageController {

    private final NcpApiService ncpApiService;

    /**
     * 블록스토리지 인스턴스 목록 조회
     * @param blockStorageInstanceListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/instances")
    public Mono<Map<String, Object>> blockStorageInstances(@RequestBody BlockStorageInstanceListRequest blockStorageInstanceListRequest){
        log.debug("##### blockStorageInstanceListRequest : {}", blockStorageInstanceListRequest.getBlockStorageInstanceNoList());
        final ResponseData responseData = new ResponseData();
        try{
            final String path = VpcServerStorageApiInfos.VPC_SERVER_STORAGE_INSTANCE_LIST.getPath();
            BlockStorageInstanceListResponse blockStorageInstanceListResponse
                    = ncpApiService.apiGetExecute(blockStorageInstanceListRequest, BlockStorageInstanceListResponse.class, path);
            responseData.responseSuccess(blockStorageInstanceListResponse);
        }catch (Exception e){
            log.debug("##### BlockStorage Instances Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
