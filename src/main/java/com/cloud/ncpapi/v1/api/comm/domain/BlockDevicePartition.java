package com.cloud.ncpapi.v1.api.comm.domain;

import lombok.Data;

@Data
public class BlockDevicePartition {

    private String mountPoint;

    private String partitionSize;
}
