package com.cloud.ncpapi.v1.api.server.server.domain.response.loginkey;

import lombok.Data;

import java.util.List;

@Data
public class LoginKeyList {

    private Integer totalRows;

    private List<LoginKey> loginKeyList;
}
