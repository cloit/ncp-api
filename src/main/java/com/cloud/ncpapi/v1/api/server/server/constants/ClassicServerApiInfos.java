package com.cloud.ncpapi.v1.api.server.server.constants;

import lombok.Getter;

public enum ClassicServerApiInfos {
    CLASSIC_SERVER_INSTANCE_LIST(
            "CLASSIC_SERVER_INSTANCE_LIST"
            , "서버(classic) 인스턴스 목록"
            , "https://ncloud.apigw.ntruss.com"
            , "/server/v2/getServerInstanceList"
            , "GET"
    )
    ;
    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    ClassicServerApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
