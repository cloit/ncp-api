package com.cloud.ncpapi.v1.api.networking.loadbalancer.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class LoadBalancerListenerListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String loadBalancerInstanceNo;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
