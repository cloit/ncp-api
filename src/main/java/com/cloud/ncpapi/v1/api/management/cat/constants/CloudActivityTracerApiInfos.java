package com.cloud.ncpapi.v1.api.management.cat.constants;

import lombok.Getter;

public enum CloudActivityTracerApiInfos {

    ACTIVITY_LIST(
            "ACTIVITY_LIST"
            , "Activity Tracer 조회"
            , "https://cloudactivitytracer.apigw.ntruss.com"
            , "/api/v1/activities"
            , "GET"
    )
    ;
    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    CloudActivityTracerApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
