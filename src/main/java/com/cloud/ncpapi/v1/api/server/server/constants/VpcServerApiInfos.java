package com.cloud.ncpapi.v1.api.server.server.constants;

import lombok.Getter;

public enum VpcServerApiInfos {

    VPC_SERVER_INSTANCE_LIST(
            "VPC_SERVER_INSTANCE_LIST"
            , "서버 인스턴스 목록"
            , "https://ncloud.apigw.ntruss.com"
            , "/vserver/v2/getServerInstanceList"
            , "GET"
    ),
    VPC_PLACEMENT_GROUP_LIST(
            "VPC_PLACEMENT_GROUP_LIST"
            , "서버 인스턴스가 소속되는 물리 배치 그룹 리스트 조회"
            , "https://ncloud.apigw.ntruss.com"
            , "/vserver/v2/getPlacementGroupList"
            , "GET"
    ),
    VPC_LOGIN_KEY_LIST(
            "VPC_LOGIN_KEY_LIST"
            , "서버 인스턴스에 접속시 로그인키를 이용하여 비밀번호를 암호하하고 복호화하는 키를 조회"
            , "https://ncloud.apigw.ntruss.com"
            , "/vserver/v2/getLoginKeyList"
            , "GET"
    ),
    VPC_CREATE_SERVER_INSTANCES(
            "VPC_CREATE_SERVER_INSTANCES"
            , "서버 인스턴스 생성"
            , "https://ncloud.apigw.ntruss.com"
            , "/vserver/v2/createServerInstances"
            , "GET"
    ),
    VPC_START_SERVER_INSTANCE(
            "VPC_START_SERVER_INSTANCE"
            , "서버 인스턴스 시작"
            , "https://ncloud.apigw.ntruss.com"
            , "/vserver/v2/startServerInstances"
            , "GET"
    )
    ;

    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    VpcServerApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
