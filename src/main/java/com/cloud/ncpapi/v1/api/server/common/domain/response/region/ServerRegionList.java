package com.cloud.ncpapi.v1.api.server.common.domain.response.region;

import lombok.Data;

import java.util.List;

@Data
public class ServerRegionList {

    private Integer totalRows;

    private List<ServerRegion> regionList;
}
