package com.cloud.ncpapi.v1.api.networking.nacl.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class VpcNaclListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String networkAclName;

    private String networkAclStatusCode;

    private List<String> networkAclNoList;

    private Integer pageNo;

    private Integer pageSize;

    private String vpcNo;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
