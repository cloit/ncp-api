package com.cloud.ncpapi.v1.api.server.common.domain.response.raid;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

@Data
public class Raid {

    private String raidTypeName;

    private String raidName;

    private CommonCode productType;
}
