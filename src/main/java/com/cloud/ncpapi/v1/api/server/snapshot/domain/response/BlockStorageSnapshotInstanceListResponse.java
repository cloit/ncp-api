package com.cloud.ncpapi.v1.api.server.snapshot.domain.response;

import lombok.Data;

@Data
public class BlockStorageSnapshotInstanceListResponse {

    private BlockStorageSnapshotInstanceList getBlockStorageSnapshotInstanceListResponse;
}
