package com.cloud.ncpapi.v1.comm.domain;

import lombok.Data;

@Data
public class NcpApiKeyInfo {

    /**
     * NCP API Access Key
     */
    private String accessKey;

    /**
     * NCP API Secret Key
     */
    private String secretKey;

    public NcpApiKeyInfo(){}

    public NcpApiKeyInfo(String accessKey, String secretKey){
        this.accessKey = accessKey;
        this.secretKey = secretKey;
    }
}
