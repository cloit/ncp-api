package com.cloud.ncpapi.v1.api.storage.nas.domain.response;

import lombok.Data;

@Data
public class NasVolumeInstanceListResponse {

    private NasVolumeInstanceList getNasVolumeInstanceListResponse;
}
