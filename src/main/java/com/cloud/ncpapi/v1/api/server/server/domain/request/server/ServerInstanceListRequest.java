package com.cloud.ncpapi.v1.api.server.server.domain.request.server;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class ServerInstanceListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String vpcNo;

    private String serverName;

    private String serverInstanceStatusCode;

    private String baseBlockStorageDiskTypeCode;

    private String baseBlockStorageDiskDetailTypeCode;

    private String ip;

    private Integer pageNo;

    private Integer pageSize;

    private String sortedBy;

    private String sortingOrder;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
