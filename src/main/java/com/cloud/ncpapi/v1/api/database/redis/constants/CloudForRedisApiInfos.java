package com.cloud.ncpapi.v1.api.database.redis.constants;

import lombok.Getter;

public enum CloudForRedisApiInfos {

    CLOUD_REDIS_INSTANCE_LIST(
            "CLOUD_REDIS_INSTANCE_LIST"
            , "REDIS 인스턴스 목록"
            , "https://ncloud.apigw.ntruss.com"
            , "/vredis/v2/getCloudRedisInstanceList"
            , "GET"
    ),
    CLOUD_REDIS_INSTANCE_DETAILS(
            "CLOUD_REDIS_INSTANCE_DETAILS"
            , "REDIS 인스턴스 목록"
            , "https://ncloud.apigw.ntruss.com"
            , "/vredis/v2/getCloudRedisInstanceDetail"
            , "GET"
    )
    ;
    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    CloudForRedisApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
