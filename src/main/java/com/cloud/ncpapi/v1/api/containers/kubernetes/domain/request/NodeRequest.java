package com.cloud.ncpapi.v1.api.containers.kubernetes.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class NodeRequest extends NcpApiKeyInfo {

    private String hypervisorCode;
}
