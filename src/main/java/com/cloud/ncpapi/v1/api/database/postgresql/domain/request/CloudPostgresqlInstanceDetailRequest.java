package com.cloud.ncpapi.v1.api.database.postgresql.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class CloudPostgresqlInstanceDetailRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String cloudPostgresqlInstanceNo;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
