package com.cloud.ncpapi.v1.api.containers.kubernetes.web;

import com.cloud.ncpapi.v1.api.comm.data.ResponseData;
import com.cloud.ncpapi.v1.api.containers.kubernetes.constants.KubernetesApiInfos;
import com.cloud.ncpapi.v1.api.containers.kubernetes.domain.request.KubernetesRequest;
import com.cloud.ncpapi.v1.api.containers.kubernetes.domain.request.NodeRequest;
import com.cloud.ncpapi.v1.api.containers.kubernetes.domain.response.KubernetesClustersResponse;
import com.cloud.ncpapi.v1.api.containers.kubernetes.domain.response.KubernetesNodePoolResponse;
import com.cloud.ncpapi.v1.api.containers.kubernetes.domain.response.KubernetesWorkerNodeResponse;
import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import com.cloud.ncpapi.v1.comm.service.NcpApiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/ncp/k8s")
@RequiredArgsConstructor
@Slf4j
public class KubernetesController {

    private final NcpApiService ncpApiService;

    /**
     * 쿠버네티스 클러스터 목록 조회
     * @param kubernetesRequest 요청파라미터
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/clusters")
    public Mono<Map<String, Object>> kubernetesClusters(@RequestBody KubernetesRequest kubernetesRequest){
        log.debug("##### kubernetesRequest : {}", kubernetesRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final String path = KubernetesApiInfos.KUBERNETES_CLUSTERS.getPath();
            final KubernetesClustersResponse kubernetesClustersResponse
                    = ncpApiService.k8sApiGetExecute(kubernetesRequest, KubernetesClustersResponse.class, path, null);
            responseData.responseSuccess(kubernetesClustersResponse);

        }catch (Exception e){
            log.debug("##### Kubernetes Clusters Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    /**
     * 쿠버네티스 노드풀 조회
     * @param uuid 노드풀 uuid
     * @param kubernetesRequest 노드풀 파라미터
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/clusters/{uuid}/node-pool")
    public Mono<Map<String, Object>> kubernetesNodePool(@PathVariable("uuid") String uuid, @RequestBody KubernetesRequest kubernetesRequest){
        log.debug("##### uuid : {}", uuid);
        log.debug("##### kubernetesRequest : {}", kubernetesRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final String path = KubernetesApiInfos.KUBERNETES_CLUSTERS_NODE_POOL.getPath();
            Map<String, String> pathParam = new HashMap<>();
            pathParam.put("uuid", URLEncoder.encode(uuid, StandardCharsets.UTF_8));
            final KubernetesNodePoolResponse kubernetesNodePoolResponse
                    = ncpApiService.k8sApiGetExecute(kubernetesRequest, KubernetesNodePoolResponse.class, path, pathParam);
            responseData.responseSuccess(kubernetesNodePoolResponse);
        }catch (Exception e){
            log.debug("##### Kubernetes Node Pool Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    /**
     * 쿠버네티스 노드 목록 조회
     * @param uuid 노드풀 uuid
     * @param kubernetesRequest 요청 파라미터
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/clusters/{uuid}/nodes")
    public Mono<Map<String, Object>> kubernetesWorkerNodes(@PathVariable("uuid") String uuid, @RequestBody KubernetesRequest kubernetesRequest){
        log.debug("##### uuid : {}", uuid);
        log.debug("##### kubernetesRequest : {}", kubernetesRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final String path = KubernetesApiInfos.KUBERNETES_CLUSTERS_NODES.getPath();
            Map<String, String> pathParam = new HashMap<>();
            pathParam.put("uuid", URLEncoder.encode(uuid, StandardCharsets.UTF_8));
            final KubernetesWorkerNodeResponse kubernetesWorkerNodeResponse
                    = ncpApiService.k8sApiGetExecute(kubernetesRequest, KubernetesWorkerNodeResponse.class, path, pathParam);
            responseData.responseSuccess(kubernetesWorkerNodeResponse);
        }catch (Exception e){
            log.debug("##### Kubernetes Node Pool Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
