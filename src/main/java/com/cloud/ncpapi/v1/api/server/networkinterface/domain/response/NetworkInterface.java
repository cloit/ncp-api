package com.cloud.ncpapi.v1.api.server.networkinterface.domain.response;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.List;

@Data
public class NetworkInterface {

    private String networkInterfaceNo;

    private String networkInterfaceName;

    private String subnetNo;

    private Boolean deleteOnTermination;

    private Boolean isDefault;

    private String deviceName;

    private CommonCode networkInterfaceStatus;

    private CommonCode instanceType;

    private String instanceNo;

    private String ip;

    private String macAddress;

    private List<String> accessControlGroupNoList;

    private String networkInterfaceDescription;

    private List<String> secondaryIpList;
}
