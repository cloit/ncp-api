package com.cloud.ncpapi.v1.api.database.mysql.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class CloudMysqlInstanceDetailRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String cloudMysqlInstanceNo;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
