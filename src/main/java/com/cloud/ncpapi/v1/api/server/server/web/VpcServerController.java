package com.cloud.ncpapi.v1.api.server.server.web;

import com.cloud.ncpapi.comm.utils.ObjectMapperUtils;
import com.cloud.ncpapi.v1.api.comm.data.ResponseData;
import com.cloud.ncpapi.v1.api.server.server.constants.VpcServerApiInfos;
import com.cloud.ncpapi.v1.api.server.server.domain.request.loginkey.LoginKeyListRequest;
import com.cloud.ncpapi.v1.api.server.server.domain.request.placementgroup.PlacementGroupListRequest;
import com.cloud.ncpapi.v1.api.server.server.domain.request.server.CreateServerInstanceRequest;
import com.cloud.ncpapi.v1.api.server.server.domain.request.server.ServerInstanceListRequest;
import com.cloud.ncpapi.v1.api.server.server.domain.request.server.StartServerInstanceRequest;
import com.cloud.ncpapi.v1.api.server.server.domain.response.loginkey.LoginKeyListResponse;
import com.cloud.ncpapi.v1.api.server.server.domain.response.placementgroup.PlacementGroupListResponse;
import com.cloud.ncpapi.v1.api.server.server.domain.response.server.CreateServerInstanceResponse;
import com.cloud.ncpapi.v1.api.server.server.domain.response.server.ServerInstanceListResponse;
import com.cloud.ncpapi.v1.api.server.server.domain.response.server.StartServerInstanceResponse;
import com.cloud.ncpapi.v1.api.server.server.service.VpcServerService;
import com.cloud.ncpapi.v1.comm.service.NcpApiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/vpc/server")
@RequiredArgsConstructor
@Slf4j
public class VpcServerController {

    private final VpcServerService vpcServerService;

    private final NcpApiService ncpApiService;

    /**
     * vpc 서버 인스터스 조회
     * @param serverInstanceListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/instances")
    public Mono<Map<String, Object>> serverInstances(@RequestBody ServerInstanceListRequest serverInstanceListRequest){
        log.debug("##### serverInstanceListRequest : {}", serverInstanceListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
//            ServerInstanceListResponse serverInstanceListResponse = vpcServerService.getServerInstanceList(serverInstanceListRequest);
            ServerInstanceListResponse serverInstanceListResponse
                    = ncpApiService.apiGetExecute(serverInstanceListRequest, ServerInstanceListResponse.class, VpcServerApiInfos.VPC_SERVER_INSTANCE_LIST.getPath());
            responseData.responseSuccess(serverInstanceListResponse);
//            responseData.responseSuccess(ObjectMapperUtils.objectToMap(serverInstanceListResponse));
        }catch (Exception e){
            log.debug("##### Vpc ServerInstances Exception", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    /**
     * 서버 인스턴스가 소속되는 물리 배치 그룹 리스트 조회
     * @param placementGroupListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/placement/groups")
    public Mono<Map<String, Object>> placementGroups(@RequestBody PlacementGroupListRequest placementGroupListRequest){
        log.debug("##### placementGroupListRequest : {}", placementGroupListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final PlacementGroupListResponse placementGroupListResponse
                    = vpcServerService.getPlacementGroupList(placementGroupListRequest);
            responseData.responseSuccess(placementGroupListResponse);
        }catch (Exception e){
            log.debug("##### Placement Groups Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    /**
     * 복호화 키 조회
     * @param loginKeyListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/login/keys")
    public Mono<Map<String, Object>> loginKeys(@RequestBody LoginKeyListRequest loginKeyListRequest){
        log.debug("##### loginKeyListRequest : {}", loginKeyListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final LoginKeyListResponse loginKeyListResponse
                    = vpcServerService.getLoginKeyList(loginKeyListRequest);
            responseData.responseSuccess(loginKeyListResponse);
        }catch (Exception e){
            log.debug("##### Login Keys Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    /**
     * 서버 인스턴스 생성
     * @param createServerInstanceRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/instances/create")
    public Mono<Map<String, Object>> serverInstancesCreate(@RequestBody CreateServerInstanceRequest createServerInstanceRequest){
        log.debug("##### createServerInstanceRequest : {}", createServerInstanceRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final CreateServerInstanceResponse createServerInstanceResponse
                    = vpcServerService.createServerInstances(createServerInstanceRequest);
            responseData.responseSuccess(createServerInstanceResponse);
//            responseData.responseSuccess(RequestDataConverter.requestDataConverter(createServerInstanceRequest));
        }catch (Exception e){
            log.debug("##### Create Server Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    /**
     * 서버 인스턴스 시작
     * @param startServerInstanceRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/instances/start")
    public Mono<Map<String, Object>> serverInstancesStart(@RequestBody StartServerInstanceRequest startServerInstanceRequest){
        log.debug("##### startServerInstanceRequest : {}", startServerInstanceRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final StartServerInstanceResponse startServerInstanceResponse
                    = vpcServerService.startServerInstance(startServerInstanceRequest);
            responseData.responseSuccess(startServerInstanceResponse);
        }catch (Exception e){
            log.debug("##### Server Instances Start Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
