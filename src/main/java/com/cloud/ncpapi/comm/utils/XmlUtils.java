package com.cloud.ncpapi.comm.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.lang.reflect.Type;

public class XmlUtils {

    /**
     * XML String -> Object
     * @param xmlString xml 데이터
     * @param destinationType 변환될 객체 타입
     * @return T
     */
    public static <T> T fromXml(String xmlString, Type destinationType) throws JsonProcessingException {
        final XmlMapper xmlMapper = new XmlMapper();
        TypeFactory typeFactory = xmlMapper.getTypeFactory();
        return xmlMapper.readValue(xmlString, typeFactory.constructType(destinationType));
    }
}
