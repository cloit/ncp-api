package com.cloud.ncpapi.v1.api.server.common.domain.response.server.image;

import com.cloud.ncpapi.v1.api.server.common.domain.response.server.comm.ProductList;
import lombok.Data;

@Data
public class ServerImageProductListResponse {

    private ProductList getServerImageProductListResponse;
}
