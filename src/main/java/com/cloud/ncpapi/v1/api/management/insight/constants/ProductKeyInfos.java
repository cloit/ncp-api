package com.cloud.ncpapi.v1.api.management.insight.constants;

import lombok.Getter;

import java.util.Arrays;

public enum ProductKeyInfos {

    SERVER(
            "server"
            , "460438474722512896"
            , "vpc"
    ),
    SERVER_CLASSIC(
            "server"
            , "769533687317532672"
            , "classic"
    ),
    LOAD_BALANCER(
            "loadbalancer"
            , "460438727509020672"
            , "vpc"
    ),
    LOAD_BALANCER_CLASSIC(
            "loadbalancer"
            , "219104633430347776"
            , "classic"
    ),
    OBJECT_STORAGE(
            "objectStorage"
            , "635182529925746688"
            , "vpc"
    )
    ;

    @Getter
    private final String instanceType;

    @Getter
    private final String productKey;

    @Getter
    private final String kind;

    ProductKeyInfos(String instanceType, String productKey, String kind) {
        this.instanceType = instanceType;
        this.productKey = productKey;
        this.kind = kind;
    }

    public static ProductKeyInfos find(String instanceType, String kind){
        return Arrays.stream(values())
                .filter(productKeyInfos -> productKeyInfos.instanceType.equals(instanceType) && productKeyInfos.kind.equals(kind))
                .findAny()
                .orElseThrow(() -> new RuntimeException("##### Product Key not found"));
    }
}
