package com.cloud.ncpapi.v1.api.server.networkinterface.service;

import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.v1.api.comm.data.Validation;
import com.cloud.ncpapi.v1.api.server.networkinterface.constants.NetworkInterfaceApiInfos;
import com.cloud.ncpapi.v1.api.server.networkinterface.domain.request.NetworkInterfaceListRequest;
import com.cloud.ncpapi.v1.api.server.networkinterface.domain.response.NetworkInterfaceListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class NetworkInterfaceService {

    private final NcpHttpService ncpHttpService;

    @Value("${ncp.properties.api-host}")
    private String apiHost;

    /**
     * 네트워크 인터페이스 리스트 조회
     * @param networkInterfaceListRequest request 정보
     * @return NetworkInterfaceListResponse
     */
    public NetworkInterfaceListResponse getNetworkInterfaceList(NetworkInterfaceListRequest networkInterfaceListRequest){

        Validation.apiKeyCheck(networkInterfaceListRequest);

        final String path = NetworkInterfaceApiInfos.VPC_NETWORK_INTERFACE_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(networkInterfaceListRequest);

        return ncpHttpService.httpExecute(httpRequest, networkInterfaceListRequest, NetworkInterfaceListResponse.class);

    }
}
