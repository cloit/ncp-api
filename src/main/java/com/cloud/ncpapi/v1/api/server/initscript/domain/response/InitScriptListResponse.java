package com.cloud.ncpapi.v1.api.server.initscript.domain.response;

import lombok.Data;

@Data
public class InitScriptListResponse {

    private InitScriptList getInitScriptListResponse;
}
