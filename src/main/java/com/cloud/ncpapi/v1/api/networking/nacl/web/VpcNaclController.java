package com.cloud.ncpapi.v1.api.networking.nacl.web;

import com.cloud.ncpapi.v1.api.comm.data.ResponseData;
import com.cloud.ncpapi.v1.api.networking.nacl.constants.VpcNaclApiInfos;
import com.cloud.ncpapi.v1.api.networking.nacl.domain.request.VpcNaclListRequest;
import com.cloud.ncpapi.v1.api.networking.nacl.domain.response.VpcNaclListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpApiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/vpc/nacl")
@RequiredArgsConstructor
@Slf4j
public class VpcNaclController {

    private final NcpApiService ncpApiService;

    /**
     * Network Acl 조회
     * @param naclListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/list")
    public Mono<Map<String, Object>> naclList(@RequestBody VpcNaclListRequest naclListRequest){
        log.debug("##### naclListRequest : {}", naclListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final String path = VpcNaclApiInfos.VPC_NACL_NETWORK_ACL_LIST.getPath();
            VpcNaclListResponse vpcNaclListResponse
                    = ncpApiService.apiGetExecute(naclListRequest, VpcNaclListResponse.class, path);
            responseData.responseSuccess(vpcNaclListResponse);
        }catch (Exception e){
            log.debug("##### Nacl List Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
