package com.cloud.ncpapi.v1.api.server.common.web;

import com.cloud.ncpapi.v1.api.comm.data.ResponseData;
import com.cloud.ncpapi.v1.api.server.common.domain.request.raid.RaidListRequest;
import com.cloud.ncpapi.v1.api.server.common.domain.request.server.image.ServerImageProductListRequest;
import com.cloud.ncpapi.v1.api.server.common.domain.request.server.product.ServerProductListRequest;
import com.cloud.ncpapi.v1.api.server.common.domain.request.spec.ServerSpecListRequest;
import com.cloud.ncpapi.v1.api.server.common.domain.response.raid.RaidListResponse;
import com.cloud.ncpapi.v1.api.server.common.domain.response.region.ServerRegionListResponse;
import com.cloud.ncpapi.v1.api.server.common.domain.response.server.image.ServerImageProductListResponse;
import com.cloud.ncpapi.v1.api.server.common.domain.response.server.product.ServerProductListResponse;
import com.cloud.ncpapi.v1.api.server.common.domain.response.spec.ServerSpecListResponse;
import com.cloud.ncpapi.v1.api.server.common.service.ServerCommonService;
import com.cloud.ncpapi.v1.api.server.serverimage.domain.request.server.ServerImageListRequest;
import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/vpc/server/common")
@RequiredArgsConstructor
@Slf4j
public class ServerCommonController {

    private final ServerCommonService serverCommonService;

    /**
     * 서버 region 조회
     * @param ncpApiKeyInfo NCP api key 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/regions")
    public Mono<Map<String, Object>> serverRegions(@RequestBody NcpApiKeyInfo ncpApiKeyInfo){
        log.debug("##### ncpApiKeyInfo : {}", ncpApiKeyInfo.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final ServerRegionListResponse serverRegionListResponse = serverCommonService.getRegionList(ncpApiKeyInfo);
            responseData.responseSuccess(serverRegionListResponse);
        }catch (Exception e){
            log.debug("###### Server Regions Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    /**
     * 서버이미지 product code 조회
     * @param serverImageProductListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/serverImageProducts")
    public Mono<Map<String, Object>> serverImageProducts(@RequestBody ServerImageProductListRequest serverImageProductListRequest){
        log.debug("##### serverImageProductListRequest : {}", serverImageProductListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final ServerImageProductListResponse serverImageProductListResponse
                    = serverCommonService.getServerImageProductList(serverImageProductListRequest);
            responseData.responseSuccess(serverImageProductListResponse);
        }catch (Exception e){
            log.debug("##### Server Image Products Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    /**
     * Server 상품 조회
     * @param serverProductListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/serverProducts")
    public Mono<Map<String, Object>> serverProducts(@RequestBody ServerProductListRequest serverProductListRequest){
        log.debug("##### serverProductListRequest : {}", serverProductListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final ServerProductListResponse serverProductListResponse
                    = serverCommonService.getServerProductList(serverProductListRequest);
            responseData.responseSuccess(serverProductListResponse);
        }catch (Exception e){
            log.debug("##### Server products Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    /**
     * Server 스펙 목록 조회
     * @param serverSpecListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/serverSpecs")
    public Mono<Map<String, Object>> serverSpecs(@RequestBody ServerSpecListRequest serverSpecListRequest){
        log.debug("##### serverSpecListRequest : {}", serverSpecListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final ServerSpecListResponse serverSpecListResponse
                    = serverCommonService.getServerSpecList(serverSpecListRequest);
            responseData.responseSuccess(serverSpecListResponse);
        }catch (Exception e){
            log.debug("##### Server specs Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    /**
     * 사용 가능한 RAID 리스트 조회
     * @param raidListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/raids")
    public Mono<Map<String, Object>> raids(@RequestBody RaidListRequest raidListRequest){
        log.debug("##### raidListRequest : {}", raidListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final RaidListResponse raidListResponse = serverCommonService.getRaidList(raidListRequest);
            responseData.responseSuccess(raidListResponse);
        }catch (Exception e){
            log.debug("##### Raids Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
