package com.cloud.ncpapi.v1.api.server.common.domain.response.spec;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

@Data
public class ServerSpec {

    private String serverSpecCode;

    private CommonCode hypervisorType;

    private CommonCode cpuArchitectureType;

    private String generationCode;

    private Integer cpuCount;

    private Long memorySize;

    private Integer blockStorageMaxCount;

    private Long blockStorageMaxIops;

    private Long blockStorageMaxThroughput;

    private Long networkPerformance;

    private Integer networkInterfaceMaxCount;

    private Integer gpuCount;

    private String serverSpecDescription;

    private String serverProductCode;
}
