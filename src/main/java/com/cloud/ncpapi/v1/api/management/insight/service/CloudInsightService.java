package com.cloud.ncpapi.v1.api.management.insight.service;

import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.comm.utils.DateUtils;
import com.cloud.ncpapi.v1.api.management.insight.constants.Calculation;
import com.cloud.ncpapi.v1.api.management.insight.constants.CloudInsightApiInfos;
import com.cloud.ncpapi.v1.api.management.insight.constants.Interval;
import com.cloud.ncpapi.v1.api.management.insight.constants.ProductKeyInfos;
import com.cloud.ncpapi.v1.api.management.insight.domain.DataQueryParam;
import com.cloud.ncpapi.v1.api.management.insight.domain.request.DataQueryRequestMetricInfo;
import com.cloud.ncpapi.v1.api.management.insight.domain.response.DataQueryResponseMetricInfo;
import com.cloud.ncpapi.v1.api.management.insight.domain.response.QueryDataMultipleResponse;
import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import com.cloud.ncpapi.v1.comm.service.NcpHttpService;
import com.google.gson.reflect.TypeToken;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class CloudInsightService {

    private final NcpHttpService ncpHttpService;

    @Value("${ncp.properties.insight-host}")
    private String insightHost;

    /**
     * 인스턴스 메트릭 정보 조회
     * @param instanceType 인스턴스 종류
     * @param dataQueryParam 요청 파라미터
     * @return QueryDataMultipleResponse
     */
    public QueryDataMultipleResponse queryDataMultiple(String instanceType, DataQueryParam dataQueryParam) throws ParseException{

        validationCheck(dataQueryParam);

        String timeStartStr     = dataQueryParam.getTimeStart();
        String timeEndStr       = dataQueryParam.getTimeEnd();
        long timeStart;
        long timeEnd;

        if(StringUtils.isEmpty(timeStartStr) || StringUtils.isEmpty(timeEndStr)){
            Date baseDate = DateUtils.addDate(new Date(), -1);
            timeStart = DateUtils.firstTimeOfDate(baseDate).getTime();
            timeEnd = DateUtils.lastTimeOfDate(baseDate).getTime();
        }else{
            timeStart = DateUtils.getStringToDate(timeStartStr, "yyyyMMdd HH:mm:ss").getTime();
            timeEnd = DateUtils.getStringToDate(timeEndStr, "yyyyMMdd HH:mm:ss").getTime();
        }


        final String accessKey          = dataQueryParam.getAccessKey();
        final String secretKey          = dataQueryParam.getSecretKey();

        List<DataQueryRequestMetricInfo> metricInfoList = setMetricInfoList(instanceType, dataQueryParam);

        HttpRequest httpRequest = HttpRequest.create()
                .post()
                .setHost(insightHost)
                .setPath(CloudInsightApiInfos.QUERY_DATA_MULTIPLE.getPath())
                .setEntity("timeStart", timeStart)
                .setEntity("timeEnd", timeEnd)
                .setEntity("metricInfoList", metricInfoList);

        List<DataQueryResponseMetricInfo> dataQueryResponseMetricInfos
            = ncpHttpService.httpExecute(httpRequest, new NcpApiKeyInfo(accessKey, secretKey), new TypeToken<ArrayList<DataQueryResponseMetricInfo>>(){}.getType());

        QueryDataMultipleResponse queryDataMultipleResponse = new QueryDataMultipleResponse();
        queryDataMultipleResponse.setDataQueryResponseMetricInfos(dataQueryResponseMetricInfos);
        return queryDataMultipleResponse;

    }


    /**
     * queryDataMultiple 요청 파라미터 셋팅
     * @param instanceType 인스턴스 종류
     * @param dataQueryParam 요청 파라미터
     * @return List<DataQueryRequestMetricInfo>
     */
    private List<DataQueryRequestMetricInfo> setMetricInfoList(String instanceType, DataQueryParam dataQueryParam) {

        List<DataQueryRequestMetricInfo> dataQueryRequestMetricInfos = new ArrayList<>();
        final List<String> instanceNos  = dataQueryParam.getInstanceNos();
        final List<String> metrics      = dataQueryParam.getMetrics();
        // kind 파라미터가 없는 경우 default : vpc
        String kind                     = dataQueryParam.getKind();
        if( StringUtils.isEmpty(kind) ){
            kind = "vpc";
        }
        final String productKey         = ProductKeyInfos.find(instanceType, kind).getProductKey();
        String interval                 = dataQueryParam.getInterval();
        List<String> aggregations       = dataQueryParam.getAggregations();

        // interval 파라미터가 없는 경우 default : DAY1
        if( StringUtils.isEmpty(interval) ){
            interval = Interval.Day1.getCodeId();
        }

        // aggregations 파라미터가 없는 경우 default : AVG
        if( aggregations == null || aggregations.size() == 0 ){
            aggregations = new ArrayList<>();
            aggregations.add(Calculation.AVG.getCodeId());
        }

        for( String instanceNo : instanceNos ){
            for( String metric : metrics ){
                for( String aggregation : aggregations ){
                    DataQueryRequestMetricInfo dataQueryRequestMetricInfo = new DataQueryRequestMetricInfo();
                    Map<String, String> dimensions = new HashMap<>();
                    dimensions.put("instanceNo", instanceNo);

                    dataQueryRequestMetricInfo.setProdKey(productKey);
                    dataQueryRequestMetricInfo.setMetric(metric);
                    dataQueryRequestMetricInfo.setInterval(Interval.find(interval));
                    dataQueryRequestMetricInfo.setAggregation(Calculation.find(aggregation));
                    dataQueryRequestMetricInfo.setDimensions(dimensions);

                    dataQueryRequestMetricInfos.add(dataQueryRequestMetricInfo);

                }
            }
        }
        return dataQueryRequestMetricInfos;
    }

    /**
     * 필수 파라미터 유효성 검사
     * @param dataQueryParam 요청 파라미터
     */
    private void validationCheck(DataQueryParam dataQueryParam){
        final String accessKey          = dataQueryParam.getAccessKey();
        final String secretKey          = dataQueryParam.getSecretKey();
        final String kind               = dataQueryParam.getKind();
        final List<String> metrics      = dataQueryParam.getMetrics();
        final List<String> instanceNos  = dataQueryParam.getInstanceNos();

        if( StringUtils.isEmpty(accessKey) ){
            throw new RuntimeException("Access Key is empty");
        }

        if( StringUtils.isEmpty(secretKey) ){
            throw new RuntimeException("Access Key is empty");
        }

        if( kind == null ){
            throw new RuntimeException("Kind is empty");
        }

        if( metrics.size() == 0 ){
            throw new RuntimeException("Metrics is empty");
        }

        if( instanceNos.size() == 0 ){
            throw new RuntimeException("instanceNo is empty");
        }
    }
}
