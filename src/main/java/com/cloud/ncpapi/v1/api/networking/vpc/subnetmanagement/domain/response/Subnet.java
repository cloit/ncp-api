package com.cloud.ncpapi.v1.api.networking.vpc.subnetmanagement.domain.response;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.Date;

@Data
public class Subnet {

    private String subnetNo;

    private String vpcNo;

    private String regionCode;

    private String zoneCode;

    private String subnetName;

    private String subnet;

    private CommonCode subnetStatus;

    private Date createDate;

    private CommonCode subnetType;

    private CommonCode usageType;

    private String networkAclNo;
}
