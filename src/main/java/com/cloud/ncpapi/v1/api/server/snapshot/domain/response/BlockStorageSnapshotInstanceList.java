package com.cloud.ncpapi.v1.api.server.snapshot.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class BlockStorageSnapshotInstanceList {

    private Integer totalRows;

    private List<BlockStorageSnapshotInstance> blockStorageSnapshotInstanceList;
}
