package com.cloud.ncpapi.v1.api.database.redis.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class CloudRedisInstanceList {

    private Integer totalRows;

    private List<CloudRedisInstance> cloudRedisInstanceList;
}
