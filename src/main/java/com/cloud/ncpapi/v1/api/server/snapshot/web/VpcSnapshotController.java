package com.cloud.ncpapi.v1.api.server.snapshot.web;

import com.cloud.ncpapi.v1.api.comm.data.ResponseData;
import com.cloud.ncpapi.v1.api.server.snapshot.domain.request.BlockStorageSnapshotInstanceListRequest;
import com.cloud.ncpapi.v1.api.server.snapshot.domain.response.BlockStorageSnapshotInstanceListResponse;
import com.cloud.ncpapi.v1.api.server.snapshot.service.VpcSnapshotService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/ncp/vpc/server")
@Slf4j
public class VpcSnapshotController {

    private final VpcSnapshotService vpcSnapshotService;

    /**
     * 블록 스토리지 인스턴스 조회
     * @param blockStorageSnapshotInstanceListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/snapshot/instances")
    public Mono<Map<String, Object>> snapshotInstances(@RequestBody BlockStorageSnapshotInstanceListRequest blockStorageSnapshotInstanceListRequest){
        log.debug("##### blockStorageSnapshotInstanceListRequest : {}", blockStorageSnapshotInstanceListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final BlockStorageSnapshotInstanceListResponse blockStorageSnapshotInstanceListResponse
                    = vpcSnapshotService.getBlockStorageSnapshotInstanceList(blockStorageSnapshotInstanceListRequest);
            responseData.responseSuccess(blockStorageSnapshotInstanceListResponse);
        }catch (Exception e){
            log.debug("##### Snapshot Instances Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
