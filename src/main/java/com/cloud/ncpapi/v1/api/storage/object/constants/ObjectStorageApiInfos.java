package com.cloud.ncpapi.v1.api.storage.object.constants;

import lombok.Getter;

public enum ObjectStorageApiInfos {

    LIST_BUCKETS(
            "LIST_BUCKETS"
            , "버킷 목록 조회"
            , "https://kr.object.ncloudstorage.com"
            , "/"
            , "GET"
    )
    ;

    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    ObjectStorageApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
