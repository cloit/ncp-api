package com.cloud.ncpapi.v1.api.server.storage.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class BlockStorageInstanceListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String zoneCode;

    private List<String> blockStorageInstanceNoList;

    private String blockStorageInstanceStatusCode;

    private String blockStorageDiskTypeCode;

    private String blockStorageDiskDetailTypeCode;

    private Integer blockStorageSize;

    private List<String> blockStorageTypeCodeList;

    private String serverInstanceNo;

    private String blockStorageName;

    private String serverName;

    private String connectionInfo;

    private List<String> blockStorageVolumeTypeCodeList;

    private List<String> hypervisorTypeCodeList;

    private Integer pageNo;

    private Integer pageSize;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
