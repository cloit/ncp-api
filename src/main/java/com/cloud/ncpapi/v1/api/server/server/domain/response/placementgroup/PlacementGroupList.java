package com.cloud.ncpapi.v1.api.server.server.domain.response.placementgroup;

import lombok.Data;

import java.util.List;

@Data
public class PlacementGroupList {

    private Integer totalRows;

    private List<PlacementGroup> placementGroupList;
}
