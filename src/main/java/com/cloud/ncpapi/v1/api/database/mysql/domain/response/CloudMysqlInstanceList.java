package com.cloud.ncpapi.v1.api.database.mysql.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class CloudMysqlInstanceList {

    private Integer totalRows;

    private List<CloudMysqlInstance> cloudMysqlInstanceList;
}
