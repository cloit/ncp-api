package com.cloud.ncpapi.v1.comm.service;

import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.comm.component.http.domain.HttpResponse;
import com.cloud.ncpapi.comm.component.http.gateway.Gateway;
import com.cloud.ncpapi.comm.utils.GsonUtils;
import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.Map;

@Service
@Slf4j
public class NcpHttpService {

    private final Gateway<NcpApiKeyInfo> gateway;

    public NcpHttpService(@Qualifier("ncpGateway") Gateway<NcpApiKeyInfo> gateway) {
        this.gateway = gateway;
    }

    public <T> T httpExecute(HttpRequest httpRequest, NcpApiKeyInfo ncpApiKeyInfo, Type type){
        Map<String, Object> queryParam = httpRequest.getQueryParams();
        queryParam.remove("accessKey");
        queryParam.remove("secretKey");
        httpRequest.setQueryParams(queryParam);

        final HttpResponse httpResponse = gateway.transmit(httpRequest, ncpApiKeyInfo);
        final int statusCode = httpResponse.getStatusCode();
        final String headerString = ArrayUtils.toString(httpResponse.getHeaders());
        final String responseBody = httpResponse.getEntityAsJson();

        log.debug("##### statusCode : {}", statusCode);
        log.debug("##### headerString : {}", headerString);
        log.debug("##### responseBody : {}", responseBody);

        if( statusCode != 200 ){
            final JsonElement jsonElement = JsonParser.parseString(responseBody);
            final JsonObject jsonObject = jsonElement.getAsJsonObject();

            String errorMessage = StringUtils.EMPTY;
            if( jsonObject.has("responseError") ){
                errorMessage = jsonObject.getAsJsonObject("responseError")
                        .get("returnMessage").getAsString();
            }else if( jsonObject.has("error") ){
                errorMessage = jsonObject.getAsJsonObject("error")
                        .get("details").getAsString();
            }

            throw new RuntimeException(errorMessage);
        }

        return GsonUtils.fromJson(responseBody, type);
    }
}
