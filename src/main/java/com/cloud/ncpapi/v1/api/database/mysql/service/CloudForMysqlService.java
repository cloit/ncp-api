package com.cloud.ncpapi.v1.api.database.mysql.service;

import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.v1.api.comm.data.Validation;
import com.cloud.ncpapi.v1.api.database.mysql.constants.CloudForMysqlApiInfos;
import com.cloud.ncpapi.v1.api.database.mysql.domain.request.CloudMysqlInstanceListRequest;
import com.cloud.ncpapi.v1.api.database.mysql.domain.response.CloudMysqlInstanceListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class CloudForMysqlService {

    private final NcpHttpService ncpHttpService;

    @Value("${ncp.properties.api-host}")
    private String apiHost;

    /**
     * Cloud For Mysql 인스턴스 조회
     * @param cloudMysqlInstanceListRequest request 정보
     * @return CloudMysqlInstanceListResponse
     */
    public CloudMysqlInstanceListResponse getCloudMysqlInstanceList(CloudMysqlInstanceListRequest cloudMysqlInstanceListRequest){

        Validation.apiKeyCheck(cloudMysqlInstanceListRequest);

        final String path = CloudForMysqlApiInfos.CLOUD_MYSQL_INSTANCE_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(cloudMysqlInstanceListRequest);

        return ncpHttpService.httpExecute(httpRequest, cloudMysqlInstanceListRequest, CloudMysqlInstanceListResponse.class);

    }
}
