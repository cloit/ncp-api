package com.cloud.ncpapi.v1.api.containers.kubernetes.domain.response;

import lombok.Data;

@Data
public class Node {

    private Long id;

    private String name;

    private String serverName;

    private String serverSpec;

    private String privateIp;

    private String publicIp;

    private String returnProtectionYn;

    private String status;

    private String statusCode;

    private String statusColor;

    private String statusName;

    private String serverImageName;

    private Integer cpuCount;

    private Long memorySize;

    private String softwareCode;

    private String productCode;

    private String specCode;

    private String loginKeyName;

    private String k8sStatus;

    private String dockerVersion;

    private String kernelVersion;

    private String nodePoolName;

    private Long nodePoolId;

    private String providerID;
}
