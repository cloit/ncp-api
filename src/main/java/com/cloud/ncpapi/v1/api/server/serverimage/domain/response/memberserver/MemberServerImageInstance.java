package com.cloud.ncpapi.v1.api.server.serverimage.domain.response.memberserver;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class MemberServerImageInstance {

    private String memberServerImageInstanceNo;

    private String memberServerImageName;

    private String memberServerImageDescription;

    private String originalServerInstanceNo;

    private String originalServerImageProductCode;

    private CommonCode memberServerImageInstanceStatus;

    private CommonCode memberServerImageInstanceOperation;

    private String memberServerImageInstanceStatusName;

    private Date createDate;

    private Integer memberServerImageBlockStorageTotalRows;

    private Long memberServerImageBlockStorageTotalSize;

    private CommonCode shareStatus;

    private List<String> sharedLoginIdList;
}
