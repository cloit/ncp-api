package com.cloud.ncpapi.v1.api.server.server.domain.response.loginkey;

import lombok.Data;

@Data
public class LoginKeyListResponse {

    private LoginKeyList getLoginKeyListResponse;
}
