package com.cloud.ncpapi.v1.api.database.mysql.domain.response;

import lombok.Data;

@Data
public class CloudMysqlInstanceDetailResponse {

    private CloudMysqlInstanceList getCloudMysqlInstanceDetailResponse;
}
