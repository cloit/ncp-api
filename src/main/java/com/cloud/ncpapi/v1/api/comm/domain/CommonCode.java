package com.cloud.ncpapi.v1.api.comm.domain;

import lombok.Data;

@Data
public class CommonCode {

    private String code;

    private String codeName;
}
