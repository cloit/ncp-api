package com.cloud.ncpapi.v1.api.storage.nas.constants;

import lombok.Getter;

public enum VpcNasApiInfos {

    VPC_NAS_VOLUME_INSTANCE_LIST(
            "VPC_NAS_VOLUME_INSTANCE_LIST"
            , "NAS 볼륨 인스턴스 리스트를 조회"
            , "https://ncloud.apigw.ntruss.com"
            , "/vnas/v2/getNasVolumeInstanceList"
            , "GET"
    )
    ;

    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    VpcNasApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
