package com.cloud.ncpapi.v1.api.containers.kubernetes.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class NodePool {

    private Long instanceNo;

    private String k8sVersion;

    private List<Label> labels;

    private String name;

    private Integer nodeCount;

    private List<Subnet> subnets;

    private List<Taint> taints;

    private List<Long> subnetNoList;

    private List<String> subnetNameList;

    private String softwareCode;

    private String productCode;

    private String serverSpecCode;

    private Integer storageSize;

    private String status;

    private String serverRoleId;

    private AutoScale autoscale;

}
