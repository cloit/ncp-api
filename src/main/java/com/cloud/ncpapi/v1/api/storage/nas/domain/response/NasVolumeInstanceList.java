package com.cloud.ncpapi.v1.api.storage.nas.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class NasVolumeInstanceList {

    private Integer totalRows;

    private List<NasVolumeInstance> nasVolumeInstanceList;
}
