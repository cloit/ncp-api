package com.cloud.ncpapi.v1.api.networking.targetgroup.domain.response;

import lombok.Data;

@Data
public class TargetGroupListResponse {

    private TargetGroupList getTargetGroupListResponse;
}
