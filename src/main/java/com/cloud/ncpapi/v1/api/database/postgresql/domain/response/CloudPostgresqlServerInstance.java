package com.cloud.ncpapi.v1.api.database.postgresql.domain.response;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.Date;

@Data
public class CloudPostgresqlServerInstance {

    private String cloudPostgresqlServerInstanceNo;

    private String cloudPostgresqlServerName;

    private CommonCode cloudPostgresqlServerRole;

    private String cloudPostgresqlServerInstanceStatusName;

    private CommonCode cloudPostgresqlServerInstanceStatus;

    private CommonCode cloudPostgresqlServerInstanceOperation;

    private String cloudPostgresqlProductCode;

    private String regionCode;

    private String zoneCode;

    private String vpcNo;

    private String subnetNo;

    private Boolean isPublicSubnet;

    private String publicDomain;

    private String privateDomain;

    private String privateIp;

    private CommonCode dataStorageType;

    private Boolean isStorageEncryption;

    private Long dataStorageSize;

    private Long usedDataStorageSize;

    private Integer cpuCount;

    private Long memorySize;

    private Date uptime;

    private Date createDate;
}
