package com.cloud.ncpapi.v1.api.server.common.domain.request.spec;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class ServerSpecListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String serverImageNo;

    private String zoneCode;

    private List<String> serverSpecCodeList;

    private List<String> hypervisorTypeCodeList;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
