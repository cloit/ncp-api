package com.cloud.ncpapi.v1.api.server.server.domain.request.server;

import lombok.Data;

import java.util.List;

@Data
public class CreateServerNetworkInterface {

    private Integer networkInterfaceOrder;

    private String networkInterfaceNo;

    private String subnetNo;

    private String ip;

    private List<String> accessControlGroupNoList;

}
