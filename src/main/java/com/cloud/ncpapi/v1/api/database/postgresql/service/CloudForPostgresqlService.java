package com.cloud.ncpapi.v1.api.database.postgresql.service;

import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.v1.api.comm.data.Validation;
import com.cloud.ncpapi.v1.api.database.postgresql.constants.CloudForPostgresqlApiInfos;
import com.cloud.ncpapi.v1.api.database.postgresql.domain.request.CloudPostgresqlInstanceListRequest;
import com.cloud.ncpapi.v1.api.database.postgresql.domain.response.CloudPostgresqlInstanceListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class CloudForPostgresqlService {

    private final NcpHttpService ncpHttpService;

    @Value("${ncp.properties.api-host}")
    private String apiHost;

    /**
     * postgresql 인스턴스 조회
     * @param cloudPostgresqlInstanceListRequest request 정보
     * @return CloudPostgresqlInstanceListResponse
     */
    public CloudPostgresqlInstanceListResponse getCloudPostgresqlInstanceList(CloudPostgresqlInstanceListRequest cloudPostgresqlInstanceListRequest){

        Validation.apiKeyCheck(cloudPostgresqlInstanceListRequest);

        final String path = CloudForPostgresqlApiInfos.CLOUD_POSTGRESQL_INSTANCE_LIST.getPath();
        
        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(cloudPostgresqlInstanceListRequest);
        
        return ncpHttpService.httpExecute(httpRequest, cloudPostgresqlInstanceListRequest, CloudPostgresqlInstanceListResponse.class);
    }
}
