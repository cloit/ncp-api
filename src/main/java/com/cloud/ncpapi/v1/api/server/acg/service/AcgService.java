package com.cloud.ncpapi.v1.api.server.acg.service;

import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.v1.api.comm.data.Validation;
import com.cloud.ncpapi.v1.api.server.acg.constants.AcgApiInfos;
import com.cloud.ncpapi.v1.api.server.acg.domain.request.AccessControlGroupListRequest;
import com.cloud.ncpapi.v1.api.server.acg.domain.response.AccessControlGroupListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class AcgService {

    private final NcpHttpService ncpHttpService;

    @Value("${ncp.properties.api-host}")
    private String apiHost;


    /**
     * ACG 리스트 조회
     * @param accessControlGroupListRequest request 정보
     * @return AccessControlGroupListResponse
     */
    public AccessControlGroupListResponse getAccessControlGroupList(AccessControlGroupListRequest accessControlGroupListRequest){

        Validation.apiKeyCheck(accessControlGroupListRequest);

        final String path = AcgApiInfos.ACCESS_CONTROL_GROUP_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(accessControlGroupListRequest);

        return  ncpHttpService.httpExecute(httpRequest, accessControlGroupListRequest, AccessControlGroupListResponse.class);
    }
}
