package com.cloud.ncpapi.v1.api.server.common.service;

import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.v1.api.comm.data.Validation;
import com.cloud.ncpapi.v1.api.server.common.constants.ServerCommonApiInfos;
import com.cloud.ncpapi.v1.api.server.common.domain.request.raid.RaidListRequest;
import com.cloud.ncpapi.v1.api.server.common.domain.request.server.image.ServerImageProductListRequest;
import com.cloud.ncpapi.v1.api.server.common.domain.request.server.product.ServerProductListRequest;
import com.cloud.ncpapi.v1.api.server.common.domain.request.spec.ServerSpecListRequest;
import com.cloud.ncpapi.v1.api.server.common.domain.response.raid.RaidListResponse;
import com.cloud.ncpapi.v1.api.server.common.domain.response.region.ServerRegionListResponse;
import com.cloud.ncpapi.v1.api.server.common.domain.response.server.image.ServerImageProductListResponse;
import com.cloud.ncpapi.v1.api.server.common.domain.response.server.product.ServerProductListResponse;
import com.cloud.ncpapi.v1.api.server.common.domain.response.spec.ServerSpecListResponse;
import com.cloud.ncpapi.v1.api.server.serverimage.domain.request.server.ServerImageListRequest;
import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import com.cloud.ncpapi.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ServerCommonService {

    private final NcpHttpService ncpHttpService;

    @Value("${ncp.properties.api-host}")
    private String apiHost;

    /**
     * 서버 리전 조회
     * @param ncpApiKeyInfo Ncp api key 정보
     * @return ServerRegionListResponse
     */
    public ServerRegionListResponse getRegionList(NcpApiKeyInfo ncpApiKeyInfo){

        Validation.apiKeyCheck(ncpApiKeyInfo);

        final String path = ServerCommonApiInfos.SERVER_REGION_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams("responseFormatType", "json");


        return ncpHttpService.httpExecute(httpRequest, ncpApiKeyInfo, ServerRegionListResponse.class);

    }

    /**
     * 서버 이미지 product code 조회
     * @param serverImageProductListRequest request 정보
     * @return ServerImageProductListResponse
     */
    public ServerImageProductListResponse getServerImageProductList(ServerImageProductListRequest serverImageProductListRequest){

        Validation.apiKeyCheck(serverImageProductListRequest);

        final String path = ServerCommonApiInfos.SERVER_IMAGE_PRODUCT_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(serverImageProductListRequest);

        return ncpHttpService.httpExecute(httpRequest, serverImageProductListRequest, ServerImageProductListResponse.class);
    }

    /**
     * Server 상품 조회
     * @param serverProductListRequest request 정보
     * @return ServerProductListResponse
     */
    public ServerProductListResponse getServerProductList(ServerProductListRequest serverProductListRequest){

        Validation.apiKeyCheck(serverProductListRequest);

        final String path = ServerCommonApiInfos.SERVER_PRODUCT_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(serverProductListRequest);

        return ncpHttpService.httpExecute(httpRequest, serverProductListRequest, ServerProductListResponse.class);
    }

    /**
     * Server 스펙 리스트 조회
     * @param serverSpecListRequest request 정보
     * @return ServerSpecListResponse
     */
    public ServerSpecListResponse getServerSpecList(ServerSpecListRequest serverSpecListRequest){

        Validation.apiKeyCheck(serverSpecListRequest);

        final String path = ServerCommonApiInfos.SERVER_SPEC_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(serverSpecListRequest);

        return ncpHttpService.httpExecute(httpRequest, serverSpecListRequest, ServerSpecListResponse.class);
    }

    /**
     * 사용 가능한 RAID 리스트 조회
     * @param raidListRequest request 정보
     * @return RaidListResponse
     */
    public RaidListResponse getRaidList(RaidListRequest raidListRequest){

        Validation.apiKeyCheck(raidListRequest);

        final String path = ServerCommonApiInfos.RAID_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(raidListRequest);

        return ncpHttpService.httpExecute(httpRequest, raidListRequest, RaidListResponse.class);
    }
}
