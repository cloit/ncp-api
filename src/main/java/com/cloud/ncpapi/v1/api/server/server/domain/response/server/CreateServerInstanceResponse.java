package com.cloud.ncpapi.v1.api.server.server.domain.response.server;

import lombok.Data;

@Data
public class CreateServerInstanceResponse {

    private ServerInstanceList createServerInstancesResponse;
}
