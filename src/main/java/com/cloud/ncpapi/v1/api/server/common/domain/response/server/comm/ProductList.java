package com.cloud.ncpapi.v1.api.server.common.domain.response.server.comm;

import lombok.Data;

import java.util.List;

@Data
public class ProductList {

    private Integer totalRows;

    private List<Product> productList;
}
