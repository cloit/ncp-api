package com.cloud.ncpapi.v1.api.server.initscript.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class InitScriptList {

    private Integer totalRows;

    private List<InitScript> initScriptList;
}
