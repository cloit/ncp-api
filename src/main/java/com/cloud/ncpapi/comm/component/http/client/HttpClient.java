package com.cloud.ncpapi.comm.component.http.client;


import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.comm.component.http.domain.HttpResponse;

public interface HttpClient {
    HttpResponse execute(HttpRequest request);
}
