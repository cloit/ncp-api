package com.cloud.ncpapi.v1.api.containers.kubernetes.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class Cluster {

    private Integer cpuCount;

    private Integer memorySize;

    private String status;

    private Integer nodeCount;

    private Integer nodeMaxCount;

    private String clusterType;

    private String hypervisorCode;

    private Long instanceNo;

    private Long id;

    private String acgName;

    private Long acgNo;

    private String capacity;

    private String createdAt;

    private String updatedAt;

    private String endpoint;

    private String uuid;

    private String name;

    private String k8sVersion;

    private String regionCode;

    private String kubeNetworkPlugin;

    private Long vpcNo;

    private List<Long> subnetNoList;

    private Long subnetLbNo;

    private Long lbPrivateSubnetNo;

    private String lbPrivateSubnetName;

    private Long lbPublicSubnetNo;

    private String lbPublicSubnetName;

    private String vpcName;

    private String subnetName;

    private List<String> subnetNameList;

    private String subnetLbName;

    private String loginKeyName;

    private String zoneCode;

    private Long zoneNo;

    private Log log;

    private Boolean publicNetwork;

    private Boolean iamAuth;

    private Boolean isDefaultIamAuth;

    private Boolean returnProtection;

    private List<NodePool> nodePool;

}
