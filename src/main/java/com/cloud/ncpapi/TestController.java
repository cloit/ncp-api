package com.cloud.ncpapi;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/ncp")
@Slf4j
public class TestController {

    @Value("${ncp.properties.api-host}")
    private String apiHost;

    @RequestMapping("/home")
    public Mono<String> home(){
        log.debug("##### apiHost : {}", apiHost);
        return Mono.just("HOME");
    }


    @RequestMapping("/xml")
    public Mono<Map<String, Object>> xmlData(){
        Map<String, Object> response = new HashMap<>();
        response.put("data", "test");
        response.put("length", 4);
        return Mono.just(response);
    }
}
