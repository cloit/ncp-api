package com.cloud.ncpapi.v1.api.server.storage.domain.response;

import lombok.Data;

@Data
public class BlockStorageInstanceListResponse {

    private BlockStorageInstanceList getBlockStorageInstanceListResponse;
}
