package com.cloud.ncpapi.comm.component.http.gateway;


import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.comm.component.http.domain.HttpResponse;

public interface Gateway<T> {
    void preExecute(HttpRequest httpRequest, T t);

    HttpResponse execute(HttpRequest httpRequest);

    void postExecute(HttpResponse httpResponse);

    default HttpResponse transmit(HttpRequest httpRequest, T t) {
        preExecute(httpRequest, t);
        HttpResponse httpResponse = execute(httpRequest);
        postExecute(httpResponse);
        return httpResponse;
    }
}
