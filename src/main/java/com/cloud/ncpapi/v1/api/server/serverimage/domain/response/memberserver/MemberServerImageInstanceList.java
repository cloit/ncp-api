package com.cloud.ncpapi.v1.api.server.serverimage.domain.response.memberserver;

import lombok.Data;

import java.util.List;

@Data
public class MemberServerImageInstanceList {

    private Integer totalRows;

    private List<MemberServerImageInstance> memberServerImageInstanceList;
}
