package com.cloud.ncpapi.v1.api.server.serverimage.domain.response.server;

import lombok.Data;

import java.util.List;

@Data
public class ServerImageList {

    private Integer totalRows;

    private List<ServerImage> serverImageList;
}
