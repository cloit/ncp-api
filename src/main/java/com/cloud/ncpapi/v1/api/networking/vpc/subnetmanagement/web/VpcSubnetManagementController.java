package com.cloud.ncpapi.v1.api.networking.vpc.subnetmanagement.web;

import com.cloud.ncpapi.v1.api.comm.data.ResponseData;
import com.cloud.ncpapi.v1.api.networking.vpc.subnetmanagement.constants.VpcSubnetManagementApiInfos;
import com.cloud.ncpapi.v1.api.networking.vpc.subnetmanagement.domain.request.VpcSubnetListRequest;
import com.cloud.ncpapi.v1.api.networking.vpc.subnetmanagement.domain.response.VpcSubnetListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpApiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/vpc")
@RequiredArgsConstructor
@Slf4j
public class VpcSubnetManagementController {

    private final NcpApiService ncpApiService;

    /**
     * VPC subnet 조회
     * @param vpcSubnetListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/subnets")
    public Mono<Map<String, Object>> subnets(@RequestBody VpcSubnetListRequest vpcSubnetListRequest){
        log.debug("###### vpcSubnetListRequest : {}", vpcSubnetListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final String path = VpcSubnetManagementApiInfos.VPC_SUBNET_LIST.getPath();
            final VpcSubnetListResponse vpcSubnetListResponse
                    = ncpApiService.apiGetExecute(vpcSubnetListRequest, VpcSubnetListResponse.class, path);
            responseData.responseSuccess(vpcSubnetListResponse);
        }catch (Exception e){
            log.debug("##### VPC Subnets Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
