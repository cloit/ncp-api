package com.cloud.ncpapi.v1.api.networking.vpc.vpcmanagement.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class VpcListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String vpcStatusCode;

    private String vpcName;

    private List<String> vpcNoList;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
