package com.cloud.ncpapi.v1.api.server.initscript.domain.response;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.Date;

@Data
public class InitScript {

    private String initScriptNo;

    private String initScriptName;

    private Date createDate;

    private String initScriptDescription;

    private String initScriptContent;

    private CommonCode osType;
}
