package com.cloud.ncpapi.v1.api.networking.loadbalancer.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class LoadBalancerInstanceListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String vpcNo;

    private String loadBalancerTypeCode;

    private String loadBalancerNetworkTypeCode;

    private List<String> loadBalancerInstanceNoList;

    private Integer pageNo;

    private Integer pageSize;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
