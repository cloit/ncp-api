package com.cloud.ncpapi.v1.api.server.common.domain.response.spec;

import lombok.Data;

@Data
public class ServerSpecListResponse {

    private ServerSpecList getServerSpecListResponse;
}
