package com.cloud.ncpapi.v1.api.database.postgresql.web;

import com.cloud.ncpapi.v1.api.comm.data.ResponseData;
import com.cloud.ncpapi.v1.api.database.postgresql.constants.CloudForPostgresqlApiInfos;
import com.cloud.ncpapi.v1.api.database.postgresql.domain.request.CloudPostgresqlInstanceDetailRequest;
import com.cloud.ncpapi.v1.api.database.postgresql.domain.request.CloudPostgresqlInstanceListRequest;
import com.cloud.ncpapi.v1.api.database.postgresql.domain.response.CloudPostgresqlInstanceDetailResponse;
import com.cloud.ncpapi.v1.api.database.postgresql.domain.response.CloudPostgresqlInstanceListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpApiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/ncp/cdb/postgresql")
@Slf4j
public class CloudForPostgresqlController {

    private final NcpApiService ncpApiService;

    /**
     * Postgresql 인스턴스 조회
     * @param cloudPostgresqlInstanceListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/instances")
    public Mono<Map<String, Object>> instances(@RequestBody CloudPostgresqlInstanceListRequest cloudPostgresqlInstanceListRequest){
        log.debug("##### cloudPostgresqlInstanceListRequest : {}", cloudPostgresqlInstanceListRequest);
        final ResponseData responseData = new ResponseData();
        try{
            final CloudPostgresqlInstanceListResponse cloudPostgresqlInstanceListResponse
                    = ncpApiService.apiGetExecute(cloudPostgresqlInstanceListRequest, CloudPostgresqlInstanceListResponse.class, CloudForPostgresqlApiInfos.CLOUD_POSTGRESQL_INSTANCE_LIST.getPath());
            responseData.responseSuccess(cloudPostgresqlInstanceListResponse);
        }catch (Exception e){
            log.debug("##### Postgresql Instances Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    @RequestMapping("/details")
    public Mono<Map<String, Object>> details(@RequestBody CloudPostgresqlInstanceDetailRequest cloudPostgresqlInstanceDetailRequest){
        log.debug("##### cloudPostgresqlInstanceDetailRequest : {}", cloudPostgresqlInstanceDetailRequest);
        final ResponseData responseData = new ResponseData();
        try{
            final CloudPostgresqlInstanceDetailResponse cloudPostgresqlInstanceDetailResponse
                    = ncpApiService.apiGetExecute(cloudPostgresqlInstanceDetailRequest, CloudPostgresqlInstanceDetailResponse.class, CloudForPostgresqlApiInfos.CLOUD_POSTGRESQL_INSTANCE_DETAILS.getPath());
            responseData.responseSuccess(cloudPostgresqlInstanceDetailResponse);
        }catch (Exception e){
            log.debug("##### Postgresql Instance Details Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
