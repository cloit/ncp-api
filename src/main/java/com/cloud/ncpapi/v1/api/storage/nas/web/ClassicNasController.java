package com.cloud.ncpapi.v1.api.storage.nas.web;

import com.cloud.ncpapi.v1.api.comm.data.ResponseData;
import com.cloud.ncpapi.v1.api.storage.nas.domain.response.ClassicNasVolumeInstanceListResponse;
import com.cloud.ncpapi.v1.api.storage.nas.service.ClassicNasService;
import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/classic")
@RequiredArgsConstructor
@Slf4j
public class ClassicNasController {

    private final ClassicNasService classicNasService;

    /**
     * NAS 볼륨 (classic) 인스턴스 조회
     * @param ncpApiKeyInfo NCP api key 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/nas/instances")
    public Mono<Map<String, Object>> nasInstances(@RequestBody NcpApiKeyInfo ncpApiKeyInfo){
        log.debug("##### ncpApiKeyInfo : {}", ncpApiKeyInfo.toString());
        ResponseData responseData = new ResponseData();
        try{
            ClassicNasVolumeInstanceListResponse classicNasVolumeInstanceListResponse
                    = classicNasService.getNasVolumeInstanceList(ncpApiKeyInfo);
            responseData.responseSuccess(classicNasVolumeInstanceListResponse);
        }catch (Exception e){
            log.debug("##### Classic NasInstances Exception", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
