package com.cloud.ncpapi.v1.api.server.common.domain.response.raid;

import lombok.Data;

@Data
public class RaidListResponse {

    private RaidList getRaidListResponse;
}
