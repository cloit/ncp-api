package com.cloud.ncpapi.v1.api.server.acg.domain.response;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

@Data
public class AccessControlGroup {

    private String accessControlGroupNo;

    private String accessControlGroupName;

    private Boolean isDefault;

    private String vpcNo;

    private CommonCode accessControlGroupStatus;

    private String accessControlGroupDescription;
}
