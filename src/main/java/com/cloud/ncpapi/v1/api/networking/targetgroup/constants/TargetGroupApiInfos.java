package com.cloud.ncpapi.v1.api.networking.targetgroup.constants;

import lombok.Getter;

public enum TargetGroupApiInfos {
    TARGET_GROUP_LIST(
            "TARGET_GROUP_LIST"
            , "타겟 그룹 리스트 조회"
            , "https://ncloud.apigw.ntruss.com"
            , "/vloadbalancer/v2/getTargetGroupList"
            , "GET"
    )
    ;
    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    TargetGroupApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
