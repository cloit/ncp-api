package com.cloud.ncpapi.v1.api.server.serverimage.domain.response.memberserver;

import lombok.Data;

@Data
public class MemberServerImageInstanceListResponse {

    private MemberServerImageInstanceList getMemberServerImageInstanceListResponse;
}
