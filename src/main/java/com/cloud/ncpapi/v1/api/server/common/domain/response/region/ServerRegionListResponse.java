package com.cloud.ncpapi.v1.api.server.common.domain.response.region;

import lombok.Data;

@Data
public class ServerRegionListResponse {

    private ServerRegionList getRegionListResponse;
}
