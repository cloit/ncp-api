package com.cloud.ncpapi.v1.api.server.serverimage.service;

import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.v1.api.comm.data.Validation;
import com.cloud.ncpapi.v1.api.server.serverimage.constants.VpcServerImageApiInfos;
import com.cloud.ncpapi.v1.api.server.serverimage.domain.request.memberserver.MemberServerImageInstanceListRequest;
import com.cloud.ncpapi.v1.api.server.serverimage.domain.request.server.ServerImageListRequest;
import com.cloud.ncpapi.v1.api.server.serverimage.domain.response.memberserver.MemberServerImageInstanceListResponse;
import com.cloud.ncpapi.v1.api.server.serverimage.domain.response.server.ServerImageListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class VpcServerImageService {

    private final NcpHttpService ncpHttpService;

    @Value("${ncp.properties.api-host}")
    private String apiHost;

    /**
     * member server image product code 조회
     * @param memberServerImageInstanceListRequest request 정보
     * @return MemberServerImageInstanceListResponse
     */
    public MemberServerImageInstanceListResponse getMemberServerImageInstanceList(MemberServerImageInstanceListRequest memberServerImageInstanceListRequest){

        Validation.apiKeyCheck(memberServerImageInstanceListRequest);

        final String path = VpcServerImageApiInfos.VPC_MEMBER_SERVER_IMAGE_INSTANCE_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(memberServerImageInstanceListRequest);

        return ncpHttpService.httpExecute(httpRequest, memberServerImageInstanceListRequest, MemberServerImageInstanceListResponse.class);
    }

    /**
     * 서버 이미지 리스트 조회
     * @param serverImageListRequest request 정보
     * @return ServerImageListResponse
     */
    public ServerImageListResponse getServerImageList(ServerImageListRequest serverImageListRequest){

        Validation.apiKeyCheck(serverImageListRequest);

        final String path = VpcServerImageApiInfos.VPC_SERVER_IMAGE_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(serverImageListRequest);

        return ncpHttpService.httpExecute(httpRequest, serverImageListRequest, ServerImageListResponse.class);

    }
}
