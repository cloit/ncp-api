package com.cloud.ncpapi.v1.api.storage.nas.domain.response;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import com.cloud.ncpapi.v1.api.comm.domain.Region;
import com.cloud.ncpapi.v1.api.comm.domain.Zone;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ClassicNasVolumeInstance {

    private String nasVolumeInstanceNo;

    private CommonCode nasVolumeInstanceStatus;

    private CommonCode nasVolumeInstanceOperation;

    private String nasVolumeInstanceStatusName;

    private Date createDate;

    private String nasVolumeInstanceDescription;

    private String mountInformation;

    private CommonCode volumeAllotmentProtocolType;

    private String volumeName;

    private long volumeTotalSize;

    private long volumeSize;

    private float snapshotVolumeConfigurationRatio;

    private CommonCode snapshotVolumeConfigPeriodType;

    private CommonCode snapshotVolumeConfigDayOfWeekType;

    private Integer snapshotVolumeConfigTime;

    private long snapshotVolumeSize;

    private boolean isSnapshotConfiguration;

    private boolean isEventConfiguration;

    private Region region;

    private Zone zone;

    private Boolean isReturnProtection;

    private List<NasVolumeInstanceCustomIp> nasVolumeInstanceCustomIpList;

    private List<String> nasVolumeServerInstanceList;
}
