package com.cloud.ncpapi.v1.api.server.acg.web;

import com.cloud.ncpapi.v1.api.comm.data.ResponseData;
import com.cloud.ncpapi.v1.api.server.acg.domain.request.AccessControlGroupListRequest;
import com.cloud.ncpapi.v1.api.server.acg.domain.response.AccessControlGroupListResponse;
import com.cloud.ncpapi.v1.api.server.acg.service.AcgService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/vpc/server/acg")
@RequiredArgsConstructor
@Slf4j
public class AcgController {

    private final AcgService acgService;

    /**
     * ACG 리스트 조회
     * @param accessControlGroupListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/accessControlGroupList")
    public Mono<Map<String, Object>> accessControlGroupList(@RequestBody AccessControlGroupListRequest accessControlGroupListRequest){
        log.debug("##### accessControlGroupListRequest : {}", accessControlGroupListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final AccessControlGroupListResponse accessControlGroupListResponse
                    = acgService.getAccessControlGroupList(accessControlGroupListRequest);
            responseData.responseSuccess(accessControlGroupListResponse);
        }catch (Exception e){
            log.debug("##### Acg list Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
