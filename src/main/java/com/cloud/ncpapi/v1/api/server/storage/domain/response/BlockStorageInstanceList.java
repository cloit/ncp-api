package com.cloud.ncpapi.v1.api.server.storage.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class BlockStorageInstanceList {

    private Integer totalRows;

    private List<BlockStorageInstance> blockStorageInstanceList;
}
