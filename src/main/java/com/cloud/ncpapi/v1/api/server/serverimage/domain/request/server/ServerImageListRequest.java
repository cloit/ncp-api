package com.cloud.ncpapi.v1.api.server.serverimage.domain.request.server;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;


@EqualsAndHashCode(callSuper = true)
@Data
public class ServerImageListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private List<String> serverImageNoList;

    private String serverImageName;

    private String serverImageStatusCode;

    private List<String> serverImageTypeCodeList;

    private List<String> hypervisorTypeCodeList;

    private List<String> osTypeCodeList;

    private List<String> platformCategoryCodeList;

    private Integer pageNo;

    private Integer pageSize;

    private String sortedBy;

    private String sortingOrder;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
