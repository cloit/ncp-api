package com.cloud.ncpapi.v1.api.storage.nas.web;

import com.cloud.ncpapi.v1.api.comm.data.ResponseData;
import com.cloud.ncpapi.v1.api.storage.nas.constants.VpcNasApiInfos;
import com.cloud.ncpapi.v1.api.storage.nas.domain.request.NasVolumeInstanceListRequest;
import com.cloud.ncpapi.v1.api.storage.nas.domain.response.NasVolumeInstanceListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpApiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/vpc")
@RequiredArgsConstructor
@Slf4j
public class VpcNasController {

    private final NcpApiService ncpApiService;

    /**
     * NAS 볼륨 인스턴스 조회
     * @param nasVolumeInstanceListRequest NAS 요청 파라미터
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/nas/instances")
    public Mono<Map<String, Object>> nasInstances(@RequestBody NasVolumeInstanceListRequest nasVolumeInstanceListRequest){
        log.debug("##### nasVolumeInstanceListRequest : {}", nasVolumeInstanceListRequest.toString());
        ResponseData responseData = new ResponseData();
        try{
            NasVolumeInstanceListResponse nasVolumeInstanceListResponse
                    = ncpApiService.apiGetExecute(nasVolumeInstanceListRequest, NasVolumeInstanceListResponse.class, VpcNasApiInfos.VPC_NAS_VOLUME_INSTANCE_LIST.getPath());
            responseData.responseSuccess(nasVolumeInstanceListResponse);
        }catch (Exception e){
            log.debug("##### Vpc NasInstances Exception", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
