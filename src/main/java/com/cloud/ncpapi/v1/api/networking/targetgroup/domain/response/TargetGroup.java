package com.cloud.ncpapi.v1.api.networking.targetgroup.domain.response;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class TargetGroup {

    private String targetGroupNo;

    private String targetGroupName;

    private CommonCode targetType;

    private String vpcNo;

    private CommonCode targetGroupProtocolType;

    private Integer targetGroupPort;

    private String targetGroupDescription;

    private Boolean useStickySession;

    private Boolean useProxyProtocol;

    private CommonCode algorithmType;

    private Date createDate;

    private String regionCode;

    private String loadBalancerInstanceNo;

    private CommonCode healthCheckProtocolType;

    private Integer healthCheckPort;

    private String healthCheckUrlPath;

    private CommonCode healthCheckHttpMethodType;

    private Integer healthCheckCycle;

    private Integer healthCheckUpThreshold;

    private Integer healthCheckDownThreshold;

    private List<String> targetNoList;
}
