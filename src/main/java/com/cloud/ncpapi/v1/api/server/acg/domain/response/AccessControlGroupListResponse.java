package com.cloud.ncpapi.v1.api.server.acg.domain.response;

import lombok.Data;

@Data
public class AccessControlGroupListResponse {

    private AccessControlGroupList getAccessControlGroupListResponse;
}
