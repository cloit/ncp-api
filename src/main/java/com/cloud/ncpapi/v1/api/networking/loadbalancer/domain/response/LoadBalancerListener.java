package com.cloud.ncpapi.v1.api.networking.loadbalancer.domain.response;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.List;

@Data
public class LoadBalancerListener {

    private String loadBalancerInstanceNo;

    private String loadBalancerListenerNo;

    private CommonCode protocolType;

    private Integer port;

    private Boolean useHttp2;

    private String sslCertificateNo;

    private CommonCode tlsMinVersionType;

    private List<String> loadBalancerRuleNoList;

    private List<String> cipherSuiteList;
}
