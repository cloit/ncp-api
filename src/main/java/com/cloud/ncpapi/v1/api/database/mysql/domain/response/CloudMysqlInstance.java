package com.cloud.ncpapi.v1.api.database.mysql.domain.response;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class CloudMysqlInstance {

    private String cloudMysqlInstanceNo;

    private String cloudMysqlServiceName;

    private String cloudMysqlInstanceStatusName;

    private CommonCode cloudMysqlInstanceStatus;

    private CommonCode cloudMysqlInstanceOperation;

    private String cloudMysqlImageProductCode;

    private String engineVersion;

    private CommonCode license;

    private Integer cloudMysqlPort;

    private Boolean isHa;

    private Boolean isMultiZone;

    private Boolean isBackup;

    private Integer backupFileRetentionPeriod;

    private String backupTime;

    private String generationCode;

    private Date createDate;

    private List<String> accessControlGroupNoList;

    private List<String> cloudMysqlConfigList;

    private List<CloudMysqlServerInstance> cloudMysqlServerInstanceList;
}
