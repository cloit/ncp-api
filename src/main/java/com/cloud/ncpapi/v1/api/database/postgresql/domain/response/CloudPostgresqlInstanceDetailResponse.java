package com.cloud.ncpapi.v1.api.database.postgresql.domain.response;

import lombok.Data;

@Data
public class CloudPostgresqlInstanceDetailResponse {

    private CloudPostgresqlInstanceList getCloudPostgresqlInstanceDetailResponse;
}
