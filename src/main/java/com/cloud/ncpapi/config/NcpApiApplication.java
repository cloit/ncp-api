package com.cloud.ncpapi.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.cloud.ncpapi")
public class NcpApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(NcpApiApplication.class, args);
    }

}
