package com.cloud.ncpapi.v1.comm.service;

import com.cloud.ncpapi.comm.component.http.client.HttpClient;
import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.comm.component.http.domain.HttpResponse;
import com.cloud.ncpapi.comm.component.http.gateway.Gateway;
import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import io.micrometer.common.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@Service
@Slf4j
public class NcpGateway implements Gateway<NcpApiKeyInfo> {

    private static final String SPACE = " ";

    private static final String LINE_SEPARATOR = "\n";

    private static final Charset UTF_8 = StandardCharsets.UTF_8;

    private static final String ALGORITHM = "HmacSHA256";

    private final HttpClient httpClient;

    public NcpGateway(@Qualifier("poolingHttpClient") HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    @Override
    public void preExecute(HttpRequest httpRequest, NcpApiKeyInfo ncpApiKeyInfo) {
        final String accessKey = ncpApiKeyInfo.getAccessKey();
        final String secretKey = ncpApiKeyInfo.getSecretKey();

        final String method = httpRequest.getHttpMethod().name();
        String path = httpRequest.getURI().getPath();
        final String query = httpRequest.getURI().getQuery();

        if (StringUtils.isNotEmpty(query)) {
            path = path + "?" + query;
        }

        final String timestamp = String.valueOf(System.currentTimeMillis());
        final String signature = makeSignature(accessKey, secretKey, method, path, timestamp);

        log.debug("##### accessKey : {}", accessKey);
        log.debug("##### secretKey : {}", secretKey);
        log.debug("##### method : {}", method);
        log.debug("##### path : {}", path);
        log.debug("##### x-ncp-apigw-timestamp : {}", timestamp);
        log.debug("##### x-ncp-iam-access-key : {}", accessKey);
        log.debug("##### x-ncp-apigw-signature-v2 : {}", signature);

        httpRequest.setHeader("x-ncp-apigw-timestamp", timestamp);
        httpRequest.setHeader("x-ncp-iam-access-key", accessKey);
        httpRequest.setHeader("x-ncp-apigw-signature-v2", signature);
    }

    @Override
    public HttpResponse execute(HttpRequest httpRequest) {
        return httpClient.execute(httpRequest);
    }

    @Override
    public void postExecute(HttpResponse httpResponse) {

    }

    private String makeSignature(String accessKey, String secretKey, String method, String url, String timestamp){
        try{
            final String message = method + SPACE + url + LINE_SEPARATOR + timestamp + LINE_SEPARATOR + accessKey;

            final SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(UTF_8), ALGORITHM);
            final Mac mac = Mac.getInstance(ALGORITHM);
            mac.init(signingKey);

            final byte[] rawHmac = mac.doFinal(message.getBytes(UTF_8));

            return Base64.encodeBase64String(rawHmac);
        }catch (Exception e){
            log.debug("##### Failed to make signature", e);
            throw new RuntimeException(e);
        }
    }
}
