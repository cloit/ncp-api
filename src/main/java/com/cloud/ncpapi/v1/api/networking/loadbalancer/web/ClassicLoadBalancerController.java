package com.cloud.ncpapi.v1.api.networking.loadbalancer.web;

import com.cloud.ncpapi.v1.api.comm.data.ResponseData;
import com.cloud.ncpapi.v1.api.networking.loadbalancer.domain.request.LoadBalancerInstanceListRequest;
import com.cloud.ncpapi.v1.api.networking.loadbalancer.domain.response.LoadBalancerInstanceListResponse;
import com.cloud.ncpapi.v1.api.networking.loadbalancer.service.ClassicLoadBalancerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/classic")
@RequiredArgsConstructor
@Slf4j
public class ClassicLoadBalancerController {

    private final ClassicLoadBalancerService classicLoadBalancerService;

    /**
     * 로드밸런서 목록 조회
     * @param loadBalancerInstanceListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/loadbalancer/instances")
    public Mono<Map<String, Object>> loadBalancerInstances(@RequestBody LoadBalancerInstanceListRequest loadBalancerInstanceListRequest){
        log.debug("##### loadBalancerInstanceListRequest : {}", loadBalancerInstanceListRequest.toString());
        ResponseData responseData = new ResponseData();
        try{
            LoadBalancerInstanceListResponse loadBalancerInstanceListResponse
                    = classicLoadBalancerService.getLoadBalancerInstanceList(loadBalancerInstanceListRequest);
            responseData.responseSuccess(loadBalancerInstanceListResponse);
        }catch (Exception e){
            log.debug("##### Classic LoadBalancer Instances Exception", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
