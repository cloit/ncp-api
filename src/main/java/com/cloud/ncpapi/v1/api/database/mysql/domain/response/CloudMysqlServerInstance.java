package com.cloud.ncpapi.v1.api.database.mysql.domain.response;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.Date;

@Data
public class CloudMysqlServerInstance {

    private String cloudMysqlServerInstanceNo;

    private String cloudMysqlServerName;

    private CommonCode cloudMysqlServerRole;

    private String cloudMysqlServerInstanceStatusName;

    private CommonCode cloudMysqlServerInstanceStatus;

    private CommonCode cloudMysqlServerInstanceOperation;

    private String cloudMysqlProductCode;

    private String regionCode;

    private String zoneCode;

    private String vpcNo;

    private String subnetNo;

    private Boolean isPublicSubnet;

    private String publicDomain;

    private String privateDomain;

    private String privateIp;

    private CommonCode dataStorageType;

    private Boolean isStorageEncryption;

    private Long dataStorageSize;

    private Long usedDataStorageSize;

    private Integer cpuCount;

    private Long memorySize;

    private Date uptime;

    private Date createDate;
}
