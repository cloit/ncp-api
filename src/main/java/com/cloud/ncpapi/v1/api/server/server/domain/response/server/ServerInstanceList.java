package com.cloud.ncpapi.v1.api.server.server.domain.response.server;

import lombok.Data;

import java.util.List;

@Data
public class ServerInstanceList {

    private Integer totalRows;

    private List<ServerInstance> serverInstanceList;

}
