//package com.cloud.ncpapi.config;
//
//import com.cloud.ncpapi.comm.utils.GsonUtils;
//import com.cloud.ncpapi.comm.utils.ObjectMapperUtils;
//import io.micrometer.common.util.StringUtils;
//import io.netty.buffer.UnpooledByteBufAllocator;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.core.annotation.Order;
//import org.springframework.core.io.buffer.*;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.MediaType;
//import org.springframework.http.server.reactive.ServerHttpRequest;
//import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
//import org.springframework.stereotype.Component;
//import org.springframework.web.server.ServerWebExchange;
//import org.springframework.web.server.WebFilter;
//import org.springframework.web.server.WebFilterChain;
//import reactor.core.publisher.Flux;
//import reactor.core.publisher.Mono;
//
//import java.nio.charset.StandardCharsets;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@Component
//@Order(1)
//@Slf4j
//public class NcpApiFilter implements WebFilter {
//
//    @Override
//    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
//        return exchange.getRequest().getBody()
//                .collectList()
//                .flatMap(data -> {
//
//                    MediaType producesMediaType = MediaType.APPLICATION_XML;
//
//                    ServerHttpRequest mutatedRequest = exchange.getRequest().mutate()
//                            .headers(headers -> headers.setAccept(Collections.singletonList(producesMediaType)))
//                            .build();
//                    return chain.filter(exchange.mutate().request(mutatedRequest).build());
//                });
//    }
//
//    private String extractRequestBody(List<DataBuffer> bodyDataBuffers) {
//        StringBuilder requestBodyBuilder = new StringBuilder();
//        for (DataBuffer buffer : bodyDataBuffers) {
//            // 버퍼를 문자열로 변환
//            byte[] bytes = new byte[buffer.readableByteCount()];
//            buffer.read(bytes);
//            DataBufferUtils.release(buffer);
//            requestBodyBuilder.append(new String(bytes, StandardCharsets.UTF_8));
//        }
//        return requestBodyBuilder.toString();
//    }
//
//    private DataBuffer stringToDataBuffer(String value) {
//        byte[] bytes = value.getBytes(StandardCharsets.UTF_8);
//        NettyDataBufferFactory nettyDataBufferFactory = new NettyDataBufferFactory(new UnpooledByteBufAllocator(false));
//        DataBuffer buffer = nettyDataBufferFactory.allocateBuffer(bytes.length);
//        buffer.write(bytes);
//        return buffer;
//    }
//
//    private boolean hasXmlData(String requestData){
//        Map<String, Object> responseMap = GsonUtils.fromJson(requestData, HashMap.class);
//        String responseFormatType = responseMap.get("responseFormatType").toString();
//        return StringUtils.isEmpty(responseFormatType) && responseFormatType.equals("xml");
//    }
//
//    private DataBuffer copyBuffer(DataBuffer buffer) {
//        NettyDataBufferFactory nettyDataBufferFactory = new NettyDataBufferFactory(new UnpooledByteBufAllocator(false));
//        byte[] bytes = new byte[buffer.readableByteCount()];
//        buffer.read(bytes);
//        DataBufferUtils.release(buffer);
//        return nettyDataBufferFactory.wrap(bytes);
//    }
//}
