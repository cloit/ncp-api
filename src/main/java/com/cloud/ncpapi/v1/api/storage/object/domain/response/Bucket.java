package com.cloud.ncpapi.v1.api.storage.object.domain.response;

import lombok.Data;

@Data
public class Bucket {

    private String name;

    private String creationDate;
}
