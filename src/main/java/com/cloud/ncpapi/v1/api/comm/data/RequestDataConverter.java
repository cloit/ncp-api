package com.cloud.ncpapi.v1.api.comm.data;

import com.cloud.ncpapi.v1.api.comm.constants.ConverterClass;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
public class RequestDataConverter {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static Object requestDataConverter(Object obj) {
        ConverterClass converterClass = ConverterClass.findConvertClass(obj.getClass());
        if( converterClass == null ){
            return obj;
        }else {
            return requestDataToMap(converterClass, obj);
        }
    }

    private static Map<String, Object> requestDataToMap(ConverterClass converterClass, Object obj) {
        Map<String, Object> converterMap = new LinkedHashMap<>();

        Map<String, Object> dataMap = objectMapper.convertValue(obj, new TypeReference<>(){});
        for( String key : dataMap.keySet() ){
            Object value = dataMap.get(key);

            if( value == null ) continue;
            if( converterClass.getConverterPropertyNames().contains(key) ){
                converterMap.putAll(listIndexingData(key, value));
            }else{
                converterMap.put(key, value);
            }
        }
        return converterMap;
    }

    private static Map<String, Object> listIndexingData(String fieldName, Object listObj) {
        Map<String, Object> resultMap = new LinkedHashMap<>();
        ArrayList<?> list = (ArrayList<?>) listObj;

        int index = 0;
        for( Object obj : list ){
            index++;
            if( isWrapperClass(obj) ){
                resultMap.putAll(listWrapperClassConverter(obj, fieldName, index));
            }else{
                resultMap.putAll(listDataClassConverter(obj, fieldName, index));
            }
        }
        return resultMap;
    }

    private static Map<String, Object> listDataClassConverter(Object dataObject, String fieldName, int index) {
        Map<String, Object> resultMap = new LinkedHashMap<>();
        Map<String, Object> dataMap = objectMapper.convertValue(dataObject, new TypeReference<>(){});
        for( String key : dataMap.keySet() ){
            String newFieldName = fieldName + "." + index + "." + key;
            Object fieldValue = dataMap.get(key);
            if( fieldValue == null ) continue;
            if( fieldValue instanceof ArrayList<?> ){
                resultMap.putAll(listIndexingData(newFieldName, fieldValue));
            }else{
                resultMap.put(newFieldName, fieldValue);
            }
        }
        return resultMap;
    }

    private static Map<String, Object> listWrapperClassConverter(Object wrapperObject, String filedName, int index){
        Map<String, Object> resultMap = new LinkedHashMap<>();
        String newFieldName = filedName + "." + index;
        resultMap.put(newFieldName, wrapperObject);
        return resultMap;
    }

    private static boolean isWrapperClass(Object obj) {
        return (obj instanceof String ||
                obj instanceof Boolean ||
                obj instanceof Byte ||
                obj instanceof Short ||
                obj instanceof Integer ||
                obj instanceof Long ||
                obj instanceof Float ||
                obj instanceof Double ||
                obj instanceof Character);
    }
}
