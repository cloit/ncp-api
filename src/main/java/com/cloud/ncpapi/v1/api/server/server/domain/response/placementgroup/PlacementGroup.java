package com.cloud.ncpapi.v1.api.server.server.domain.response.placementgroup;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

@Data
public class PlacementGroup {

    private String placementGroupNo;

    private String placementGroupName;

    private CommonCode placementGroupType;
}
