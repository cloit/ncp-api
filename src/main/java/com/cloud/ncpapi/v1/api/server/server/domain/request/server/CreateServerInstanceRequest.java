package com.cloud.ncpapi.v1.api.server.server.domain.request.server;

import com.cloud.ncpapi.v1.api.comm.domain.BlockDevicePartition;
import com.cloud.ncpapi.v1.api.comm.domain.BlockStorageMapping;
import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class CreateServerInstanceRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String memberServerImageInstanceNo;

    private String serverImageProductCode;

    private String serverImageNo;

    private String vpcNo;

    private String subnetNo;

    private String serverProductCode;

    private String serverSpecCode;

    private Boolean isEncryptedBaseBlockStorageVolume;

    private String feeSystemTypeCode;

    private Integer serverCreateCount;

    private Integer serverCreateStartNo;

    private String serverName;

    private List<CreateServerNetworkInterface> networkInterfaceList;

    private String placementGroupNo;

    private Boolean isProtectServerTermination;

    private String serverDescription;

    private String initScriptNo;

    private String loginKeyName;

    private Boolean associateWithPublicIp;

    private String raidTypeName;

    private List<BlockDevicePartition> blockDevicePartitionList;

    private List<BlockStorageMapping> blockStorageMappingList;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
