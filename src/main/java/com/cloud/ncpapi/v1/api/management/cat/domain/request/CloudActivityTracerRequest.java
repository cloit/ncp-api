package com.cloud.ncpapi.v1.api.management.cat.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class CloudActivityTracerRequest extends NcpApiKeyInfo {

    private Integer fromEventTime;

}
