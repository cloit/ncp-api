package com.cloud.ncpapi.v1.api.management.insight.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class QueryDataMultipleResponse {

    private List<DataQueryResponseMetricInfo> dataQueryResponseMetricInfos;
}
