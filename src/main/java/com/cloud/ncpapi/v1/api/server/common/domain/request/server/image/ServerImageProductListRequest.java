package com.cloud.ncpapi.v1.api.server.common.domain.request.server.image;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class ServerImageProductListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String blockStorageSize;

    private String exclusionProductCode;

    private String productCode;

    private List<String> platformTypeCodeList;

    private String infraResourceDetailTypeCode;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
