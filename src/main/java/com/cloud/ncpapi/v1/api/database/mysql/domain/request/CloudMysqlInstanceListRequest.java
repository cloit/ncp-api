package com.cloud.ncpapi.v1.api.database.mysql.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class CloudMysqlInstanceListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String zoneCode;

    private String vpcNo;

    private String subnetNo;

    private String cloudMysqlServiceName;

    private List<String> cloudMysqlInstanceNoList;

    private String cloudMysqlServerName;

    private List<String> cloudMysqlServerInstanceNoList;

    private String generationCode;

    private Integer pageNo;

    private Integer pageSize;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
