package com.cloud.ncpapi.v1.api.server.serverimage.web;

import com.cloud.ncpapi.v1.api.comm.data.ResponseData;
import com.cloud.ncpapi.v1.api.server.serverimage.domain.request.memberserver.MemberServerImageInstanceListRequest;
import com.cloud.ncpapi.v1.api.server.serverimage.domain.request.server.ServerImageListRequest;
import com.cloud.ncpapi.v1.api.server.serverimage.domain.response.memberserver.MemberServerImageInstanceListResponse;
import com.cloud.ncpapi.v1.api.server.serverimage.domain.response.server.ServerImageListResponse;
import com.cloud.ncpapi.v1.api.server.serverimage.service.VpcServerImageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/vpc/server")
@RequiredArgsConstructor
@Slf4j
public class VpcServerImageController {

    private final VpcServerImageService vpcServerImageService;

    /**
     * member server image product code 조회
     * @param memberServerImageInstanceListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/member/image/instances")
    public Mono<Map<String, Object>> memberServerImageInstances(@RequestBody MemberServerImageInstanceListRequest memberServerImageInstanceListRequest){
        log.debug("##### memberServerImageInstanceListRequest : {}", memberServerImageInstanceListRequest.toString());
        ResponseData responseData = new ResponseData();
        try{
            MemberServerImageInstanceListResponse memberServerImageInstanceListResponse
                    = vpcServerImageService.getMemberServerImageInstanceList(memberServerImageInstanceListRequest);
            responseData.responseSuccess(memberServerImageInstanceListResponse);
        }catch (Exception e){
            log.debug("##### Member Server Image Instances Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    /**
     * 서버 이미지 목록 조회
     * @param serverImageListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/images")
    public Mono<Map<String, Object>> serverImages(@RequestBody ServerImageListRequest serverImageListRequest){
        log.debug("##### serverImageListRequest : {}", serverImageListRequest.toString());
        ResponseData responseData = new ResponseData();
        try{
            ServerImageListResponse serverImageListResponse
                    = vpcServerImageService.getServerImageList(serverImageListRequest);
            responseData.responseSuccess(serverImageListResponse);
        }catch (Exception e){
            log.debug("##### Server Images Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
