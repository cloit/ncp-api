package com.cloud.ncpapi.v1.api.database.redis.domain.response;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.Date;

@Data
public class CloudRedisServerInstance {

    private String cloudRedisServerInstanceNo;

    private String cloudRedisServerName;

    private CommonCode cloudRedisServerRole;

    private String cloudRedisServerInstanceStatusName;

    private CommonCode CloudRedisServerInstanceStatus;

    private CommonCode CloudRedisServerInstanceOperation;

    private String cloudRedisProductCode;

    private String regionCode;

    private String zoneCode;

    private String vpcNo;

    private String subnetNo;

    private String privateDomain;

    private Integer cpuCount;

    private Long memorySize;

    private Long osMemorySize;

    private Date uptime;

    private Date createDate;

    private String slotName;
}
