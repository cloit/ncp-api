package com.cloud.ncpapi.v1.api.networking.targetgroup.web;

import com.cloud.ncpapi.v1.api.comm.data.ResponseData;
import com.cloud.ncpapi.v1.api.networking.targetgroup.constants.TargetGroupApiInfos;
import com.cloud.ncpapi.v1.api.networking.targetgroup.domain.request.TargetGroupListRequest;
import com.cloud.ncpapi.v1.api.networking.targetgroup.domain.response.TargetGroupListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpApiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/vpc")
@RequiredArgsConstructor
@Slf4j
public class TargetGroupController {

    private final NcpApiService ncpApiService;

    @RequestMapping("/targetGroup/list")
    public Mono<Map<String, Object>> targetGroupList(@RequestBody TargetGroupListRequest targetGroupListRequest) {
        log.debug("##### targetGroupListRequest : {}", targetGroupListRequest.toString());
        ResponseData responseData = new ResponseData();
        try{
            final TargetGroupListResponse targetGroupListResponse
                    = ncpApiService.apiGetExecute(targetGroupListRequest, TargetGroupListResponse.class, TargetGroupApiInfos.TARGET_GROUP_LIST.getPath());
            responseData.responseSuccess(targetGroupListResponse);
        }catch (Exception e){
            log.debug("##### Vpc LoadBalancerInstances Exception", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
