package com.cloud.ncpapi.v1.api.storage.object.web;

import com.cloud.ncpapi.v1.api.comm.data.ResponseData;
import com.cloud.ncpapi.v1.api.storage.object.domain.ObjectStorageAuthInfo;
import com.cloud.ncpapi.v1.api.storage.object.domain.response.ListBuckets;
import com.cloud.ncpapi.v1.api.storage.object.service.ObjectStorageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp")
@RequiredArgsConstructor
@Slf4j
public class ObjectStorageController {

    private final ObjectStorageService objectStorageService;

    /**
     * Object Storage 버킷 리스트 조회
     * @param objectStorageAuthInfo ObjectStorage 인증정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/storage/objectStorages/list/buckets")
    public Mono<Map<String, Object>> listBuckets(@RequestBody ObjectStorageAuthInfo objectStorageAuthInfo){
        log.debug("##### objectStorageAuthInfo : {}", objectStorageAuthInfo.toString());
        ResponseData responseData = new ResponseData();
        try{
            ListBuckets listBuckets = objectStorageService.listBuckets(objectStorageAuthInfo);
            responseData.responseSuccess(listBuckets);
        }catch (Exception e){
            log.debug("##### ObjectStorages Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
