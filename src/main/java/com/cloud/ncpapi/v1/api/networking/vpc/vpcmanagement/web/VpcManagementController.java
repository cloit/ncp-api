package com.cloud.ncpapi.v1.api.networking.vpc.vpcmanagement.web;

import com.cloud.ncpapi.v1.api.comm.data.ResponseData;
import com.cloud.ncpapi.v1.api.networking.vpc.vpcmanagement.constants.VpcManagementApiInfos;
import com.cloud.ncpapi.v1.api.networking.vpc.vpcmanagement.domain.request.VpcListRequest;
import com.cloud.ncpapi.v1.api.networking.vpc.vpcmanagement.domain.response.VpcListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpApiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/vpc")
@RequiredArgsConstructor
@Slf4j
public class VpcManagementController {

    private final NcpApiService ncpApiService;

    /**
     * Vpc 목록 조회
     * @param vpcListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/list")
    public Mono<Map<String, Object>> vpcList(@RequestBody VpcListRequest vpcListRequest){
        log.debug("##### vpcListRequest : {}", vpcListRequest);
        final ResponseData responseData = new ResponseData();
        try{
            final String path = VpcManagementApiInfos.VPC_LIST.getPath();
            final VpcListResponse vpcListResponse
                    = ncpApiService.apiGetExecute(vpcListRequest, VpcListResponse.class, path);
            responseData.responseSuccess(vpcListResponse);
        }catch (Exception e){
            log.debug("##### Vpc List Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
