package com.cloud.ncpapi.v1.api.server.networkinterface.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class NetworkInterfaceListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String subnetName;

    private List<String> networkInterfaceNoList;

    private String networkInterfaceName;

    private String networkInterfaceStatusCode;

    private String ip;

    private List<String> secondaryIpList;

    private String instanceNo;

    private Boolean isDefault;

    private String deviceName;

    private String serverName;

    private Integer pageNo;

    private Integer pageSize;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
