package com.cloud.ncpapi.v1.api.server.initscript.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class InitScriptListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private List<String> initScriptNoList;

    private String initScriptName;

    private String osTypeCode;

    private Integer pageNo;

    private Integer pageSize;

    private String sortedBy;

    private String sortingOrder;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
