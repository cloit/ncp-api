package com.cloud.ncpapi.v1.api.networking.loadbalancer.web;

import com.cloud.ncpapi.v1.api.comm.data.ResponseData;
import com.cloud.ncpapi.v1.api.networking.loadbalancer.constants.VpcLoadBalancerApiInfos;
import com.cloud.ncpapi.v1.api.networking.loadbalancer.domain.request.LoadBalancerInstanceListRequest;
import com.cloud.ncpapi.v1.api.networking.loadbalancer.domain.request.LoadBalancerListenerListRequest;
import com.cloud.ncpapi.v1.api.networking.loadbalancer.domain.response.LoadBalancerInstanceListResponse;
import com.cloud.ncpapi.v1.api.networking.loadbalancer.domain.response.LoadBalancerListenerListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpApiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/vpc")
@RequiredArgsConstructor
@Slf4j
public class VpcLoadBalancerController {

    private final NcpApiService ncpApiService;

    /**
     * vpc 로드밸런서 목록 조회
     * @param loadBalancerInstanceListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/loadbalancer/instances")
    public Mono<Map<String, Object>> loadBalancerInstances(@RequestBody LoadBalancerInstanceListRequest loadBalancerInstanceListRequest){
        log.debug("##### loadBalancerInstanceListRequest : {}", loadBalancerInstanceListRequest.toString());
        ResponseData responseData = new ResponseData();
        try{
            final LoadBalancerInstanceListResponse loadBalancerInstanceListResponse
                    = ncpApiService.apiGetExecute(loadBalancerInstanceListRequest, LoadBalancerInstanceListResponse.class, VpcLoadBalancerApiInfos.VPC_LOAD_BALANCER_INSTANCE_LIST.getPath());
            responseData.responseSuccess(loadBalancerInstanceListResponse);
        }catch (Exception e){
            log.debug("##### Vpc LoadBalancerInstances Exception", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    @RequestMapping("/loadbalancer/listener/list")
    public Mono<Map<String, Object>> loadBalancerListenerList(@RequestBody LoadBalancerListenerListRequest loadBalancerListenerListRequest){
        log.debug("##### loadBalancerListenerListRequest : {}", loadBalancerListenerListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final LoadBalancerListenerListResponse loadBalancerListenerListResponse
                    = ncpApiService.apiGetExecute(loadBalancerListenerListRequest, LoadBalancerListenerListResponse.class, VpcLoadBalancerApiInfos.VPC_LOAD_BALANCER_LISTENER_LIST.getPath());
            responseData.responseSuccess(loadBalancerListenerListResponse);
        }catch (Exception e){
            log.debug("##### Vpc LoadBalancer Listener List Exception", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
