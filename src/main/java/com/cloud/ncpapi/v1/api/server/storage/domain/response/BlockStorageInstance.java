package com.cloud.ncpapi.v1.api.server.storage.domain.response;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.Date;

@Data
public class BlockStorageInstance {

    private String blockStorageInstanceNo;

    private String serverInstanceNo;

    private String blockStorageName;

    private CommonCode blockStorageType;

    private Long blockStorageSize;

    private String deviceName;

    private String blockStorageProductCode;

    private CommonCode blockStorageInstanceStatus;

    private CommonCode blockStorageInstanceOperation;

    private String blockStorageInstanceStatusName;

    private Date createDate;

    private String blockStorageDescription;

    private CommonCode blockStorageDiskType;

    private CommonCode blockStorageDiskDetailType;

    private Integer maxIopsThroughput;

    private Boolean isEncryptedVolume;

    private String zoneCode;

    private String regionCode;

    private Boolean isReturnProtection;

    private CommonCode blockStorageVolumeType;

    private CommonCode hypervisorType;

    private Long throughput;

    private Long iops;

    private Long blockStorageSnapshotInstanceNo;
}
