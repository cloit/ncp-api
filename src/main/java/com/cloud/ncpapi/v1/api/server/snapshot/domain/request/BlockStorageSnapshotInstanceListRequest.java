package com.cloud.ncpapi.v1.api.server.snapshot.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class BlockStorageSnapshotInstanceListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private List<String> blockStorageSnapshotInstanceNoList;

    private String blockStorageSnapshotName;

    private String blockStorageSnapshotInstanceStatusCode;

    private List<String> originalBlockStorageInstanceNoList;

    private Integer blockStorageSnapshotVolumeSize;

    private Boolean isEncryptedOriginalBlockStorageVolume;

    private List<String> hypervisorTypeCodeList;

    private Boolean isBootable;

    private Integer pageNo;

    private Integer pageSize;

    private String sortedBy;

    private String sortingOrder;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
