package com.cloud.ncpapi.v1.api.networking.loadbalancer.domain.response;

import lombok.Data;

@Data
public class LoadBalancerInstanceListResponse {

    private LoadBalancerInstanceList getLoadBalancerInstanceListResponse;
}
