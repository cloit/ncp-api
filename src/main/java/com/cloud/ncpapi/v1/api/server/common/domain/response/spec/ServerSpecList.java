package com.cloud.ncpapi.v1.api.server.common.domain.response.spec;

import lombok.Data;

import java.util.List;

@Data
public class ServerSpecList {

    private Integer totalRows;

    private List<ServerSpec> serverSpecList;
}
