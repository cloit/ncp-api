package com.cloud.ncpapi.v1.api.storage.object.constants;

import lombok.Getter;

import java.util.Arrays;

public enum RegionDomain {

    KR_STANDARD(
            "kr-standard"
            , "https://kr.object.gov-ncloudstorage.com"
    ),
    US_STANDARD(
            "us-standard"
            , "https://us.object.ncloudstorage.com"
    ),
    SG_STANDARD(
            "sg-standard"
            , "https://sg.object.ncloudstorage.com"
    ),
    JP_STANDARD(
            "jp-standard"
            , "https://jp.object.ncpstorage.com"
    ),
    DE_STANDARD(
            "de-standard"
            , "https://de.object.ncloudstorage.com"
    ),

    ;

    @Getter
    private final String region;

    @Getter
    private final String domain;

    RegionDomain(String region, String domain) {
        this.region = region;
        this.domain = domain;
    }

    public static RegionDomain find(String region){
        return Arrays.stream(values())
                .filter(regionDomain -> regionDomain.region.equals(region))
                .findAny()
                .orElseThrow(() -> new RuntimeException("##### Region not found"));
    }
}
