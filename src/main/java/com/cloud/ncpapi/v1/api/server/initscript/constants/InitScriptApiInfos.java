package com.cloud.ncpapi.v1.api.server.initscript.constants;

import lombok.Getter;

public enum InitScriptApiInfos {

    INIT_SCRIPT_LIST(
            "INIT_SCRIPT_LIST"
            , "사용자가 생성한 초기화 스크립트 리스트 조회"
            , "https://ncloud.apigw.ntruss.com"
            , "/vserver/v2/getInitScriptList"
            , "GET"
    )
    ;

    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    InitScriptApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
