package com.cloud.ncpapi.v1.api.storage.nas.service;

import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.v1.api.comm.data.Validation;
import com.cloud.ncpapi.v1.api.storage.nas.constants.VpcNasApiInfos;
import com.cloud.ncpapi.v1.api.storage.nas.domain.request.NasVolumeInstanceListRequest;
import com.cloud.ncpapi.v1.api.storage.nas.domain.response.NasVolumeInstanceListResponse;
import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import com.cloud.ncpapi.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class VpcNasService {

    private final NcpHttpService ncpHttpService;

    @Value("${ncp.properties.api-host}")
    private String apiHost;

    /**
     * NAS 볼륨 인스턴스 조회
     * @param ncpApiKeyInfo NCP api key 정보
     * @return NasVolumeInstanceListResponse
     */
    public NasVolumeInstanceListResponse getNasVolumeInstanceList(NcpApiKeyInfo ncpApiKeyInfo){

        Validation.apiKeyCheck(ncpApiKeyInfo);

        final String path = VpcNasApiInfos.VPC_NAS_VOLUME_INSTANCE_LIST.getPath();
        final NasVolumeInstanceListRequest nasVolumeInstanceListRequest
                = new NasVolumeInstanceListRequest();

        HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(nasVolumeInstanceListRequest);

        return ncpHttpService.httpExecute(httpRequest, ncpApiKeyInfo, NasVolumeInstanceListResponse.class);
    }

}
