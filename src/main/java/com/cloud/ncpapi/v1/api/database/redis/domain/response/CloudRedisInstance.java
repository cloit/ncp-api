package com.cloud.ncpapi.v1.api.database.redis.domain.response;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class CloudRedisInstance {

    private String cloudRedisInstanceNo;

    private String cloudRedisServiceName;

    private String cloudRedisInstanceStatusName;

    private CommonCode cloudRedisInstanceStatus;

    private CommonCode cloudRedisInstanceOperation;

    private String cloudRedisImageProductCode;

    private String engineVersion;

    private CommonCode licence;

    private Integer cloudRedisPort;

    private Boolean isHa;

    private String cloudRedisServerPrefix;

    private Boolean isBackup;

    private Integer backupFileRetentionPeriod;

    private String backupTime;

    private String backupSchedule;

    private Date createDate;

    private Integer shardCount;

    private Integer shardCopyCount;

    private List<String> accessControlGroupNoList;

    private String configGroupNo;

    private String configGroupName;

    private CommonCode role;

    private List<CloudRedisServerInstance> cloudRedisServerInstanceList;
}
