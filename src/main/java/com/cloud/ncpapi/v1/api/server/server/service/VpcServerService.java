package com.cloud.ncpapi.v1.api.server.server.service;

import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.v1.api.comm.data.RequestDataConverter;
import com.cloud.ncpapi.v1.api.comm.data.Validation;
import com.cloud.ncpapi.v1.api.server.server.constants.VpcServerApiInfos;
import com.cloud.ncpapi.v1.api.server.server.domain.request.loginkey.LoginKeyListRequest;
import com.cloud.ncpapi.v1.api.server.server.domain.request.placementgroup.PlacementGroupListRequest;
import com.cloud.ncpapi.v1.api.server.server.domain.request.server.CreateServerInstanceRequest;
import com.cloud.ncpapi.v1.api.server.server.domain.request.server.ServerInstanceListRequest;
import com.cloud.ncpapi.v1.api.server.server.domain.request.server.StartServerInstanceRequest;
import com.cloud.ncpapi.v1.api.server.server.domain.response.loginkey.LoginKeyListResponse;
import com.cloud.ncpapi.v1.api.server.server.domain.response.placementgroup.PlacementGroupListResponse;
import com.cloud.ncpapi.v1.api.server.server.domain.response.server.CreateServerInstanceResponse;
import com.cloud.ncpapi.v1.api.server.server.domain.response.server.ServerInstanceListResponse;
import com.cloud.ncpapi.v1.api.server.server.domain.response.server.StartServerInstanceResponse;
import com.cloud.ncpapi.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class VpcServerService {

    private final NcpHttpService ncpHttpService;

    @Value("${ncp.properties.api-host}")
    private String apiHost;

    /**
     * 서버 인스턴스 조회
     * @param serverInstanceListRequest request 정보
     * @return ServerInstanceListResponse
     */
    public ServerInstanceListResponse getServerInstanceList(ServerInstanceListRequest serverInstanceListRequest){

        Validation.apiKeyCheck(serverInstanceListRequest);

        final String path = VpcServerApiInfos.VPC_SERVER_INSTANCE_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(serverInstanceListRequest);

        return ncpHttpService.httpExecute(httpRequest, serverInstanceListRequest, ServerInstanceListResponse.class);

    }

    /**
     * 서버 인스턴스가 소속되는 물리 배치 그룹 리스트 조회
     * @param placementGroupListRequest request 정보
     * @return PlacementGroupListResponse
     */
    public PlacementGroupListResponse getPlacementGroupList(PlacementGroupListRequest placementGroupListRequest){

        Validation.apiKeyCheck(placementGroupListRequest);

        final String path = VpcServerApiInfos.VPC_PLACEMENT_GROUP_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(placementGroupListRequest);

        return ncpHttpService.httpExecute(httpRequest, placementGroupListRequest, PlacementGroupListResponse.class);
    }

    /**
     * 복호화 키 조회
     * @param loginKeyListRequest request 정보
     * @return LoginKeyListResponse
     */
    public LoginKeyListResponse getLoginKeyList(LoginKeyListRequest loginKeyListRequest){

        Validation.apiKeyCheck(loginKeyListRequest);

        final String path = VpcServerApiInfos.VPC_LOGIN_KEY_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(loginKeyListRequest);

        return ncpHttpService.httpExecute(httpRequest, loginKeyListRequest, LoginKeyListResponse.class);
    }

    /**
     * 서버 인스턴스 생성
     * @param createServerInstanceRequest request 정보
     * @return CreateServerInstanceResponse
     */
    public CreateServerInstanceResponse createServerInstances(CreateServerInstanceRequest createServerInstanceRequest) {

        Validation.apiKeyCheck(createServerInstanceRequest);

        final String path = VpcServerApiInfos.VPC_CREATE_SERVER_INSTANCES.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(RequestDataConverter.requestDataConverter(createServerInstanceRequest));

        return ncpHttpService.httpExecute(httpRequest, createServerInstanceRequest, CreateServerInstanceResponse.class);

    }

    /**
     * 서버 인스턴스 시작
     * @param startServerInstanceRequest request 정보
     * @return StartServerInstanceResponse
     */
    public StartServerInstanceResponse startServerInstance(StartServerInstanceRequest startServerInstanceRequest){

        Validation.apiKeyCheck(startServerInstanceRequest);

        final String path = VpcServerApiInfos.VPC_START_SERVER_INSTANCE.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(RequestDataConverter.requestDataConverter(startServerInstanceRequest));

        return ncpHttpService.httpExecute(httpRequest, startServerInstanceRequest, StartServerInstanceResponse.class);
    }

}
