package com.cloud.ncpapi.v1.api.server.networkinterface.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class NetworkInterfaceList {

    private Integer totalRows;

    private List<NetworkInterface> networkInterfaceList;
}
