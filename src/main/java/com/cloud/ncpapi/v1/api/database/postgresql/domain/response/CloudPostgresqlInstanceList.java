package com.cloud.ncpapi.v1.api.database.postgresql.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class CloudPostgresqlInstanceList {

    private Integer totalRows;

    private List<CloudPostgresqlInstance> cloudPostgresqlInstanceList;
}
