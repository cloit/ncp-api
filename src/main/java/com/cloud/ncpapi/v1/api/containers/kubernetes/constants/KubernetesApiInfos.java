package com.cloud.ncpapi.v1.api.containers.kubernetes.constants;

import lombok.Getter;

public enum KubernetesApiInfos {

    KUBERNETES_CLUSTERS(
            "KUBERNETES_CLUSTERS"
            , "쿠버네티스 클러스터 조회"
            , "https://nks.apigw.ntruss.com"
            , "/vnks/v2/clusters"
            , "GET"
    ),
    KUBERNETES_CLUSTERS_NODE_POOL(
            "KUBERNETES_CLUSTERS_NODE_POOL"
            , "노드풀 목록 조회"
            , "https://nks.apigw.ntruss.com"
            , "/vnks/v2/clusters/{uuid}/node-pool"
            , "GET"
    ),
    KUBERNETES_CLUSTERS_NODES(
            "KUBERNETES_CLUSTERS_NODES"
            , "클러스터 내 등록된 WorkerNode 목록 조회"
            , "https://nks.apigw.ntruss.com"
            , "/vnks/v2/clusters/{uuid}/nodes"
            , "GET"
    )
    ;

    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    KubernetesApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
