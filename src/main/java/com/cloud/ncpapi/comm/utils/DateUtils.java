package com.cloud.ncpapi.comm.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    /**
     * 현재날짜 문자열
     * @param pattern 날짜 포맷
     * @return String
     */
    public static String getCurrentDateStr(String pattern){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(new Date());
    }

    /**
     * 문자열 -> Date
     * @param date 날짜 문자열
     * @param pattern 날짜 포맷
     * @return Date
     */
    public static Date getStringToDate(String date, String pattern) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.parse(date);
    }

    /**
     * Date -> String
     * @param date 날짜
     * @param pattern 날짜 포맷
     * @return String
     */
    public static String getDateToString(Date date, String pattern){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }

    /**
     * 달의 첫날
     * @param date 대상날짜
     * @return Date
     */
    public static Date firstDateOfMonth(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 달의 첫째 날짜 문자열
     * @param date 대상날짜
     * @param pattern 날짜 포맷
     * @return Date
     */
    public static String firstDateOfMonthToString(Date date, String pattern){
        Date firstDate = firstDateOfMonth(date);
        return getDateToString(firstDate, pattern);
    }

    /**
     * 달의 마지막 날짜
     * @param date 대상날짜
     * @return Date
     */
    public static Date lastDateOfMonth(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int lastDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, lastDay);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 달의 마지막 날짜 문자열
     * @param date 대상날짜
     * @param pattern 날짜 포맷
     * @return String
     */
    public static String lastDateOfMonthToString(Date date, String pattern){
        Date lastDate = lastDateOfMonth(date);
        return getDateToString(lastDate, pattern);
    }

    /**
     * 대상날짜에 월 더하기 or 빼기
     * @param date 대상날짜
     * @param addMonth 더할(뺄) 월
     * @return Date
     */
    public static Date addMonth(Date date, int addMonth){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, addMonth);
        return calendar.getTime();
    }

    public static Date addDate(Date date, int addDate){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, addDate);
        return calendar.getTime();
    }

    /**
     * 해당 일자의 최초 시간 00:00:00
     * @param date 기준일
     * @return Date
     */
    public static Date firstTimeOfDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 해당 일자의 마지막 시간 23:59:59
     * @param date 기준일
     * @return Date
     */
    public static Date lastTimeOfDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
}
