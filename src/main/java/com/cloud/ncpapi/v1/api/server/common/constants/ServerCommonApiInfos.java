package com.cloud.ncpapi.v1.api.server.common.constants;

import lombok.Getter;

public enum ServerCommonApiInfos {

    SERVER_REGION_LIST(
            "SERVER_REGION_LIST"
            , "서버 리전 리스트 조회"
            , "https://ncloud.apigw.ntruss.com"
            , "/vserver/v2/getRegionList"
            , "GET"
    ),
    SERVER_IMAGE_PRODUCT_LIST(
            "SERVER_IMAGE_PRODUCT_LIST"
            , "서버 이미지 리스트 조회"
            , "https://ncloud.apigw.ntruss.com"
            , "/vserver/v2/getServerImageProductList"
            , "GET"
    ),
    SERVER_PRODUCT_LIST(
            "SERVER_PRODUCT_LIST"
            , "서버 상품 리스트 조회"
            , "https://ncloud.apigw.ntruss.com"
            , "/vserver/v2/getServerProductList"
            , "GET"
    ),
    SERVER_SPEC_LIST(
            "SERVER_SPEC_LIST"
            , "서버 스펙 리스트 조회"
            , "https://ncloud.apigw.ntruss.com"
            , "/vserver/v2/getServerSpecList"
            , "GET"
    ),
    RAID_LIST(
            "RAID_LIST"
            , "사용 가능한 RAID 리스트 조회"
            , "https://ncloud.apigw.ntruss.com"
            , "/vserver/v2/getRaidList"
            , "GET"
    )
    ;

    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    ServerCommonApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
