package com.cloud.ncpapi.comm.component.http.client;

import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.comm.component.http.domain.HttpResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RestTemplateHttpClient implements HttpClient {
    @Override
    public HttpResponse execute(HttpRequest request) {
        return null;
    }

}
