package com.cloud.ncpapi.comm.component.http.exception;

public class GatewayException extends HttpException{
    public GatewayException() {
        super();
    }

    public GatewayException(String message){
        super(message);
    }

    public GatewayException(String message, Throwable cause) {
        super(message, cause);
    }

    public GatewayException(Throwable cause) {
        super(cause);
    }

}
