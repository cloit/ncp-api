package com.cloud.ncpapi.v1.api.networking.vpc.subnetmanagement.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class VpcSubnetListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private List<String> subnetNoList;

    private String subnetName;

    private String subnet;

    private String subnetTypeCode;

    private String usageTypeCode;

    private String networkAclNo;

    private Integer pageNo;

    private Integer pageSize;

    private String subnetStatusCode;

    private String vpcNo;

    private String zoneCode;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
