package com.cloud.ncpapi.v1.api.database.postgresql.domain.response;

import com.cloud.ncpapi.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class CloudPostgresqlInstance {

    private String cloudPostgresqlInstanceNo;

    private String cloudPostgresqlServiceName;

    private String cloudPostgresqlInstanceStatusName;

    private CommonCode cloudPostgresqlInstanceStatus;

    private CommonCode cloudPostgresqlInstanceOperation;

    private String cloudPostgresqlImageProductCode;

    private String engineVersion;

    private String license;

    private Integer cloudPostgresqlPort;

    private Boolean isHa;

    private Boolean isMultiZone;

    private Boolean isBackup;

    private Integer backupFileRetentionPeriod;

    private String backupTime;

    private Date createDate;

    private List<String> accessControlGroupNoList;

    private List<String> cloudPostgresqlConfigList;

    private List<CloudPostgresqlServerInstance> cloudPostgresqlServerInstanceList;
}
