package com.cloud.ncpapi.v1.api.networking.targetgroup.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class TargetGroupList {

    private Integer totalRows;

    private List<TargetGroup> targetGroupList;
}
