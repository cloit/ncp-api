package com.cloud.ncpapi.v1.api.server.initscript.service;

import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.v1.api.comm.data.Validation;
import com.cloud.ncpapi.v1.api.server.initscript.constants.InitScriptApiInfos;
import com.cloud.ncpapi.v1.api.server.initscript.domain.request.InitScriptListRequest;
import com.cloud.ncpapi.v1.api.server.initscript.domain.response.InitScriptListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class InitScriptService {

    private final NcpHttpService ncpHttpService;

    @Value("${ncp.properties.api-host}")
    private String apiHost;

    /**
     * 초기화 스크립트 리스트 조회
     * @param initScriptListRequest request 정보
     * @return InitScriptListResponse
     */
    public InitScriptListResponse getInitScriptList(InitScriptListRequest initScriptListRequest){

        Validation.apiKeyCheck(initScriptListRequest);

        final String path = InitScriptApiInfos.INIT_SCRIPT_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(initScriptListRequest);

        return ncpHttpService.httpExecute(httpRequest, initScriptListRequest, InitScriptListResponse.class);
    }
}
