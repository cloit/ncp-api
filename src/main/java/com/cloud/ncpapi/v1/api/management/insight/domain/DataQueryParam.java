package com.cloud.ncpapi.v1.api.management.insight.domain;

import lombok.Data;

import java.util.List;

@Data
public class DataQueryParam {

    private String accessKey;

    private String secretKey;

    private String kind;

    private List<String> metrics;

    private String interval;

    private List<String> aggregations;

    private List<String> instanceNos;

    private String timeStart;

    private String timeEnd;
}
