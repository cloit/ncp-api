package com.cloud.ncpapi.v1.api.management.insight.constants;

public enum QueryAggregation {

    COUNT,
    SUM,
    MAX,
    MIN,
    AVG
}
