package com.cloud.ncpapi.v1.api.containers.kubernetes.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class KubernetesWorkerNodeResponse {

    private List<Node> nodes;
}
