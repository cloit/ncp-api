package com.cloud.ncpapi.v1.api.networking.nacl.constants;

import lombok.Getter;

public enum VpcNaclApiInfos {
    VPC_NACL_NETWORK_ACL_LIST(
            "VPC_NACL_NETWORK_ACL_LIST"
            , "Network ACL 리스트를 조회"
            , "https://ncloud.apigw.ntruss.com"
            , "/vpc/v2/getNetworkAclList"
            , "GET"
    )
    ;
    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    VpcNaclApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
