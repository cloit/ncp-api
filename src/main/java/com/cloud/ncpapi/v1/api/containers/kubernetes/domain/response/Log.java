package com.cloud.ncpapi.v1.api.containers.kubernetes.domain.response;

import lombok.Data;

@Data
public class Log {

    private Boolean audit;
}
