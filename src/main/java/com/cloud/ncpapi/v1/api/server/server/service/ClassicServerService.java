package com.cloud.ncpapi.v1.api.server.server.service;

import com.cloud.ncpapi.comm.component.http.domain.HttpRequest;
import com.cloud.ncpapi.v1.api.comm.data.Validation;
import com.cloud.ncpapi.v1.api.server.server.constants.ClassicServerApiInfos;
import com.cloud.ncpapi.v1.api.server.server.domain.request.server.ServerInstanceListRequest;
import com.cloud.ncpapi.v1.api.server.server.domain.response.server.ServerInstanceListResponse;
import com.cloud.ncpapi.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ClassicServerService {

    private final NcpHttpService ncpHttpService;

    @Value("${ncp.properties.api-host}")
    private String apiHost;

    /**
     * 서버(classic) 인스턴스 조회
     * @param serverInstanceListRequest request 정보
     * @return ServerInstanceListResponse
     */
    public ServerInstanceListResponse getServerInstanceList(ServerInstanceListRequest serverInstanceListRequest){

        Validation.apiKeyCheck(serverInstanceListRequest);

        final String path = ClassicServerApiInfos.CLASSIC_SERVER_INSTANCE_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(apiHost)
                .setPath(path)
                .setQueryParams(serverInstanceListRequest);

        return ncpHttpService.httpExecute(httpRequest, serverInstanceListRequest, ServerInstanceListResponse.class);
    }
}
