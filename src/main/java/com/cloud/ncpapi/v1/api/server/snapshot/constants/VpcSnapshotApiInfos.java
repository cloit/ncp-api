package com.cloud.ncpapi.v1.api.server.snapshot.constants;

import lombok.Getter;

public enum VpcSnapshotApiInfos {

    VPC_BLOCK_STORAGE_SNAPSHOT_INSTANCE_LIST(
            "VPC_BLOCK_STORAGE_SNAPSHOT_INSTANCE_LIST"
            , "블록 스토리지 스냅샷 인스턴스 리스트 조회"
            , "https://ncloud.apigw.ntruss.com"
            , "/vserver/v2/getBlockStorageSnapshotInstanceList"
            , "GET"
    )
    ;

    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    VpcSnapshotApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
