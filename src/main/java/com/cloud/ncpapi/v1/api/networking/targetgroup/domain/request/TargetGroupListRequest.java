package com.cloud.ncpapi.v1.api.networking.targetgroup.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class TargetGroupListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private Long vpcNo;

    private String targetTypeCode;

    private List<Long> targetGroupNoList;

    private Integer pageNo;

    private Integer pageSize;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
