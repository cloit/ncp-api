package com.cloud.ncpapi.v1.api.database.redis.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class CloudRedisInstanceDetailRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String cloudRedisInstanceNo;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
