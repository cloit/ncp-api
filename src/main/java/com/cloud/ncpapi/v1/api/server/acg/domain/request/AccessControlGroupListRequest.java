package com.cloud.ncpapi.v1.api.server.acg.domain.request;

import com.cloud.ncpapi.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
public class AccessControlGroupListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String vpcNo;

    private List<String> accessControlGroupNoList;

    private String accessControlGroupName;

    private String accessControlGroupStatusCode;

    private Integer pageNo;

    private Integer pageSize;

    private String responseFormatType;

    public String getResponseFormatType() {
        return Objects.toString(responseFormatType, "json");
    }
}
