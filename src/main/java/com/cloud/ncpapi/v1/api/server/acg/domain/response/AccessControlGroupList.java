package com.cloud.ncpapi.v1.api.server.acg.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class AccessControlGroupList {

    private Integer totalRows;

    private List<AccessControlGroup> accessControlGroupList;
}
