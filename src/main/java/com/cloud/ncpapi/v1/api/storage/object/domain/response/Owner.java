package com.cloud.ncpapi.v1.api.storage.object.domain.response;

import lombok.Data;

@Data
public class Owner {

    private String id;

    private String displayName;

}
