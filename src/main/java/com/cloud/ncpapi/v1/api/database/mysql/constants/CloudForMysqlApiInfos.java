package com.cloud.ncpapi.v1.api.database.mysql.constants;

import lombok.Getter;

public enum CloudForMysqlApiInfos {

    CLOUD_MYSQL_INSTANCE_LIST(
            "CLOUD_MYSQL_INSTANCE_LIST"
            , "MySQL 인스턴스 목록"
            , "https://ncloud.apigw.ntruss.com"
            , "/vmysql/v2/getCloudMysqlInstanceList"
            , "GET"
    ),
    CLOUD_MYSQL_INSTANCE_DETAILS(
            "CLOUD_MYSQL_INSTANCE_DETAILS"
            , "MySQL 인스턴스 상세정보"
            , "https://ncloud.apigw.ntruss.com"
            , "/vmysql/v2/getCloudMysqlInstanceDetail"
            , "GET"
    ),
    ;
    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    CloudForMysqlApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
