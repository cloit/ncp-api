package com.cloud.ncpapi.v1.api.database.redis.domain.response;

import lombok.Data;

@Data
public class CloudRedisInstanceDetailResponse {

    private CloudRedisInstanceList getCloudRedisInstanceDetailResponse;
}
