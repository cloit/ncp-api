package com.cloud.ncpapi.v1.api.server.serverimage.domain.response.server;

import lombok.Data;

@Data
public class ServerImageListResponse {

    private ServerImageList getServerImageListResponse;
}
