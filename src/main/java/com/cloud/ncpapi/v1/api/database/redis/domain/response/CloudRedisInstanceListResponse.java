package com.cloud.ncpapi.v1.api.database.redis.domain.response;

import lombok.Data;

@Data
public class CloudRedisInstanceListResponse {

    private CloudRedisInstanceList getCloudRedisInstanceListResponse;
}
