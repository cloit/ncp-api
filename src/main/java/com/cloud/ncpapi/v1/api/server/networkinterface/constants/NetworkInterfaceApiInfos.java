package com.cloud.ncpapi.v1.api.server.networkinterface.constants;

import lombok.Getter;

public enum NetworkInterfaceApiInfos {

    VPC_NETWORK_INTERFACE_LIST(
            "VPC_NETWORK_INTERFACE_LIST"
            , "네트워크 인터페이스 리스트 조회"
            , "https://ncloud.apigw.ntruss.com"
            , "/vserver/v2/getNetworkInterfaceList"
            , "GET"
    );

    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    NetworkInterfaceApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
