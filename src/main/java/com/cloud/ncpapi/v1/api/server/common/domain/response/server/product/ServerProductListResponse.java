package com.cloud.ncpapi.v1.api.server.common.domain.response.server.product;

import com.cloud.ncpapi.v1.api.server.common.domain.response.server.comm.ProductList;
import lombok.Data;

@Data
public class ServerProductListResponse {

    private ProductList getServerProductListResponse;
}
