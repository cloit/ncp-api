package com.cloud.ncpapi.v1.api.server.acg.constants;

import lombok.Getter;

public enum AcgApiInfos {

    ACCESS_CONTROL_GROUP_LIST(
            "ACCESS_CONTROL_GROUP_LIST"
            , "ACG 리스트 조회"
            , "https://ncloud.apigw.ntruss.com"
            , "/vserver/v2/getAccessControlGroupList"
            , "GET"
    )
    ;

    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    AcgApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
